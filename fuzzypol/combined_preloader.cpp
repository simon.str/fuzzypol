#include <unistd.h>
#include <ctype.h>
#include <string.h> 
#include <stdio.h> 
#include <string> 
#include <seccomp.h> 
#include <bitset> 
#include <algorithm> 
#include <iostream>
#include <fstream> 
#include <sys/prctl.h>

#define MAX_CMDLINE_LEN 1000
#define MAX_CMDLINE_PAR 1000

void init_seccomp_profile(void) {
  std::string input; 
  int syscall_counter; 
  scmp_filter_ctx ctx86_64= nullptr; 

  const char* bitvector_file_path = std::getenv("PG_BITVECTOR_FILE"); 

  if (bitvector_file_path == NULL) {
    std::_Exit(EXIT_FAILURE); 
  }
  std::ifstream bitset_syscall_mask_file(bitvector_file_path);
  if (!bitset_syscall_mask_file.is_open()) {
    std::_Exit(EXIT_FAILURE);         
  }
  bitset_syscall_mask_file >> input; 
  std::reverse(input.begin(), input.end());

  std::bitset<480> syscall_mask{input};
  printf("INSIDE FUNCTION\n");
  prctl(PR_SET_NO_NEW_PRIVS, 1); 

  ctx86_64 = seccomp_init(SCMP_ACT_KILL_PROCESS);

  seccomp_arch_remove(ctx86_64, SCMP_ARCH_NATIVE); 
  seccomp_arch_add(ctx86_64, SCMP_ARCH_X86_64);

  seccomp_rule_add(ctx86_64, SCMP_ACT_ALLOW, 30, 0);
  seccomp_rule_add(ctx86_64, SCMP_ACT_ALLOW, 31, 0);
  

  for (syscall_counter = 0; syscall_counter <= 479; syscall_counter++) {
    if (syscall_mask[syscall_counter] == 1){
        seccomp_rule_add(ctx86_64, SCMP_ACT_ALLOW, syscall_counter, 0);
    }
  }

  if (seccomp_load(ctx86_64) != 0) {
    std::_Exit(EXIT_FAILURE); 
  }
  
  printf("Successfully intialized Seccomp filters\n");
}

extern "C"
void afl_init_wrapper(char ***argv, int *argc, char *progname) 
{
  printf("OVERRIDE SUCCESSFULL\n");
  static char  in_buf[MAX_CMDLINE_LEN + 2];
  static char* ret[MAX_CMDLINE_PAR];

  char* ptr = in_buf;
  int   rc  = 1;
  ret[0] = progname;

  size_t read_result = read(0, in_buf, MAX_CMDLINE_LEN);
  if (read_result < 0){
    std::_Exit(EXIT_FAILURE); 
    //Wichtig
  };

  while (*ptr) {
    if(rc >= MAX_CMDLINE_PAR) {
            break;
    }

    ret[rc] = ptr;

    while (*ptr && !isspace(*ptr)) ptr++;
    *ptr = '\0';
    ptr++;

    while (*ptr && isspace(*ptr)) ptr++;
    rc++;
  }
  if (argc) *argc = rc;
  if (argv) *argv = ret;
  init_seccomp_profile();
}
