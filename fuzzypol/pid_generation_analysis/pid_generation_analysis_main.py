''' Module to analyze all PID Generation regarding syscalls. 

    All information are going to be exchanged with a shared memory named:
'''
import bcc 
import numpy as np 
import multiprocessing
from multiprocessing import shared_memory
import logging 
import signal 
import configparser 

interrupt= True
bpf_object: bcc.BPF 
pid_shm: shared_memory.SharedMemory
pid_list: np.ndarray
iterator = 0 
binary_basename: str


def signal_handler(singal, frame):
    global interrupt 
    interrupt = False 

signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)

def initialize_shared_memory(): 
    global pid_shm
    global pid_list 

    # array_size = np.zeros(1000, dtype=np.int64).nbytes
    # pid_shm = shared_memory.SharedMemory(name="pid_shm", 
    #                                         create= True, size= array_size)
    
    # pid_list = np.ndarray(1000, dtype=np.int64, buffer=pid_shm.buf)
    # pid_list[:] = np.zeros(1000, dtype=np.int64)[:]

    # logging.debug("Successfully initalized Shared Memory master")

    
    pid_shm = shared_memory.SharedMemory(name="pid_shm")
    pid_list = np.ndarray(1000, dtype=np.int64, buffer=pid_shm.buf)

    logging.debug("PID Generation Analysis Process attached to shared memory")



def destroy_shm(): 
    global pid_shm

    pid_shm.close() 
    logging.debug("PID Generation Analsis Process closed shared memory")


def post_process_pid_generation_syscall(event)->dict:
    result_dict = dict()

    result_dict["syscall"] = event.syscall
    result_dict["pid"] = event.pid 
    result_dict["ppid"] = event.ppid 
    result_dict["ret_val"] = event.ret_val
    result_dict["comm"] = event.comm.decode("utf-8")

    return result_dict


def analyse_pid_generation_syscall(result_dict: dict)->None:
    global pid_list 
    global iterator

    if ((result_dict['comm'] == binary_basename or
            int(result_dict['pid']) in pid_list) and 
            int(result_dict['ret_val']) > 0): 
        pid_list[iterator] = int(result_dict['ret_val'])
        logging.debug(f"Added PID {int(result_dict['ret_val'])} to PID list")

        iterator = (iterator+1)%1000

def init_pid_generation_bpf(config:configparser.ConfigParser): 
    ''' Initialize the BPF object for analyzing PID generating syscalls 
    '''

    global bpf_object

    filter_pid_generation_file = "/synced_folder/sandboxpol/ebpf/" \
        + "ebpf_filter_programs/pid_generation_bpf.c"
    with open(filter_pid_generation_file, 'r') as pid_filter: 
        ebpf_filter_text = pid_filter.read() 

    # TODO: Replace Magic String with config parameter 
    ebpf_filter_text = ebpf_filter_text.replace("UID_FILTER",
        f'if (uid != {2000}) {{return 0;}}')

    cflag_list = list()
    if config.getboolean('DEBUG_PARAMETERS', 
            'disable_macro_redefinition_warning'):
        cflag_list.append("-Wno-macro-redefined")
        logging.debug("Disabled macro warnings")
    
    debug = 0
    if config.getboolean('DEBUG_PARAMETERS', 'debug_preprocessor'):
        debug = bcc.DEBUG_PREPROCESSOR
        logging.debug("Printed Debugging Preprocessor Output")


    bpf_object = bcc.BPF(text=ebpf_filter_text, cflags=cflag_list, debug=debug)

    clone_fnname = bpf_object.get_syscall_fnname("clone")
    fork_fnname = bpf_object.get_syscall_fnname("fork")
    vfork_fnname = bpf_object.get_syscall_fnname("vfork")
    clone3_fnname = bpf_object.get_syscall_fnname("clone3")

    bpf_object.attach_kretprobe(event=clone_fnname, fn_name="do_ret_sys_clone")
    bpf_object.attach_kretprobe(event=fork_fnname, fn_name="do_ret_sys_fork")
    bpf_object.attach_kretprobe(event=vfork_fnname, fn_name="do_ret_sys_vfork")
    bpf_object.attach_kretprobe(
        event=clone3_fnname, fn_name="do_ret_sys_clone3")


def pid_generation_log_analysis(cpu, data, size):  
    global bpf_object
    event = bpf_object["pid_ring_buffer"].event(data)

    pid_result_dict = post_process_pid_generation_syscall(event)

    analyse_pid_generation_syscall(pid_result_dict)

def print_pid_list(): 
    print(pid_list)

def pid_generation_analysis_main(bpf_setup_event:multiprocessing.Event, 
        config: configparser.ConfigParser): 
    global bpf_object
    global interrupt
    global binary_basename
    

    binary_basename = config["KLEE"]["bin_name"]
    initialize_shared_memory()
    init_pid_generation_bpf(config)
    bpf_setup_event.set() 

    bpf_object["pid_ring_buffer"].open_perf_buffer(pid_generation_log_analysis, page_cnt=256)

    while interrupt:
        try:  
            bpf_object.perf_buffer_poll()
        except KeyboardInterrupt: 
            break;

    logging.debug("Recieved SIGTERM:"
        +" Terminating PID Generation Analysis Process")
    destroy_shm()

if __name__=="__main__": 
    logging.basicConfig(level=logging.DEBUG,
                        format='%(filename)s %(funcName)s: %(message)s')
    
    pid_generation_analysis_main()