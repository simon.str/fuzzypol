from multiprocessing.sharedctypes import Value
import os 

def build_PID_filter(program_text:str) -> str:
    pid_filter_string = "if ("
    pids_noisy_programs = get_pids() 

    #print(pids_noisy_programs)
    if not pids_noisy_programs: 
        program_text = program_text.replace("PID_FILTER", "") 
        return program_text
    else: 
        for item in pids_noisy_programs: 
            pid_filter_string += f"(pid == {item}) || "

    pid_filter_string = pid_filter_string[:-4]
    pid_filter_string += ") return 0;" 
    program_text = program_text.replace("PID_FILTER", pid_filter_string)

    return program_text

def get_pids():
    proc_dirs = os.listdir("/proc")
    actual_process_dirs = list() 
    for item in proc_dirs: 
        try: 
            actual_process_dirs.append(int(item))
        except ValueError:
            continue
    
    actual_process_dirs.remove(os.getpid())
    #print(f"{actual_process_dirs}\n\n{os.getpid()}")

    return actual_process_dirs 

if __name__ == "__main__": 
    print(build_PID_filter("PID_FILTER"))