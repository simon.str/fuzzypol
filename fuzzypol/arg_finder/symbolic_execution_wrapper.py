import os 
import subprocess
import glob
import shutil 

class SymbolicExecutionWrapper: 
    def __init__(self, main_script: str, bitcode: str, afl_instructed_bin: str, symargs: str, cov_dir: str, duration: str = "60"):
        '''Wrapper class for the symbolic execution of KLEE and performing tests with AFL++. 
        '''

        if os.getuid() != 0: 
            print("[!] Script requires root prvileges")
            exit(-1)

        self.main_script = main_script
        self.bitcode = bitcode
        self.afl_instructed_bin = afl_instructed_bin
        self.symargs = symargs
        self.cov_dir = cov_dir
        self.duration = duration
        self.check_file_existance()

        print("[+] Successfully initialized the Symbolic Execution Wrapper:\n")


    def check_file_existance(self):
        print(f"CWD = {os.getcwd()}")
        if not os.path.exists(self.main_script):
            print("[!] SymbolicExecutionWrapper: Main Script path does not exist.")
            exit(-1)
        if not os.path.exists(self.bitcode):
            print("[!] SymbolicExecutionWrapper: Bitcode path does not exist.") 
            exit(-1)
        if not os.path.exists(self.afl_instructed_bin): 
            print("[!] SymbolicExecutionWrapper: AFL instructed bin path does not exist.") 
            exit(-1)


    def execute(self, debug:bool = True):
        '''Execute the Symbolic Execution with AFL. 
        
        If debug is set to True any subprocess call will be displayed on stdout. 
        '''
        print("[+] Executing KLEE")
        if debug: 
            #subprocess.call(["sudo", "python3" , self.main_script, self.bitcode, self.afl_instructed_bin, self.symargs, "KS", self.cov_dir, self.duration])
            subprocess.call(["sudo", "python3" , self.main_script, self.bitcode, self.afl_instructed_bin, self.symargs, self.duration])
        else:
            #subprocess.call(["sudo", "python3" , self.main_script, self.bitcode, self.afl_instructed_bin, self.symargs, "KS", self.cov_dir, self.duration], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            subprocess.call(["sudo", "python3" , self.main_script, self.bitcode, self.afl_instructed_bin, self.symargs, self.duration], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


    def clear(self): 
        dir = os.path.abspath(os.path.join(self.main_script, os.pardir))
        del_dirs = glob.glob(f"{dir}/klee-out-*", recursive=True)
        
        bin_name = os.path.basename(self.afl_instructed_bin)
        if del_dirs: 
            del_dirs.append(f"{dir}/fuzzin.txt")
            del_dirs.append(f"{dir}/KLEEArgs1.txt")
            del_dirs.append(f"{dir}/{os.path.basename(self.afl_instructed_bin)}replay.txt")
            del_dirs.append(f"{dir}/{bin_name}_flags.txt")
            del_dirs.append(f"{dir}/{bin_name}.bc")
            del_dirs.append(f"{dir}/{bin_name}")


        for item in del_dirs: 
            if os.path.isdir(item):
                print(f"Removing dir {item}")
                try: 
                    shutil.rmtree(item)
                # Device busy
                except OSError as e: 
                    continue 
            else: 
                print(f"Removing file {item}")
                os.remove(item)



    def process_output(self): 
        basename = os.path.basename(self.afl_instructed_bin)

        base_dir = os.path.abspath(os.path.join(self.main_script, os.pardir))
        found_args = open(f"{base_dir}/fuzzin.txt", 'r')
        result_file = open(f"{basename}_flags.txt", 'w')

        for line in found_args: 
            result_file.write(f'"./ls" "{line.strip()}"\n')

        found_args.close()
        result_file.close()
  

    def get_result_file_name(self): 
        basename = os.path.basename(self.afl_instructed_bin)
        return os.path.join(os.getcwd(), f"{basename}_flags.txt")




if __name__ == "__main__": 
    symbol_execution = SymbolicExecutionWrapper("./afl_run_afl-with-cov.py", 
                            "ls.bc", "ls", "30", "./GCC/")
    symbol_execution.execute()
    symbol_execution.process_output()
    symbol_execution.clear()
                            