import io
import re
import shlex
import subprocess
import sys
import threading


cmd_argv_len1 = len(sys.argv)
cmd_argv_1 = []
cmd_argv_1 = sys.argv
KLEE_Target_Programm = str(cmd_argv_1[1])
binary = str(cmd_argv_1[2])
prog_args = str(cmd_argv_1[3])

duration = str(cmd_argv_1[4])

print(f"[+] KLEE script commandline arguments:\n\t{cmd_argv_1}")

def prepare_KLEE_env():
    try:
        print("\t(+) Unarchiving KLEE sandbox")
        KLEE_cmd_sandbox_tar = "tar -xzf sandbox.tgz"
        #print("(+) Prepare sandbox Step 2")
        KLEE_cmd_sandbox_tar_1 = shlex.split(KLEE_cmd_sandbox_tar)
        sandbox_unarchive_process = subprocess.Popen(KLEE_cmd_sandbox_tar_1)
        sandbox_unarchive_process.wait() 
        print("\t(+) Copying KLEE sandbox")
        KLEE_cmd_sandbox= "cp -r sandbox/ /tmp/"
        KLEE_cmd_sandbox_1 = shlex.split(KLEE_cmd_sandbox)
        sandbox_copy_process = subprocess.Popen(KLEE_cmd_sandbox_1)
        sandbox_copy_process.wait()
    except:
        print("(-) Failed CP sandbox")
    else:
        try:
            print("\t(+) Prepare env")
            KLEE_cmd_env_command = "env -i /bin/bash -c '(source testing-env.sh; env > test.env)' "
            KLEE_cmd_env_command_1 = shlex.split(KLEE_cmd_env_command)
            #KLEE_cmd_Run_env = subprocess.Popen(KLEE_cmd_env_command_1)
            subprocess.Popen(KLEE_cmd_env_command_1)
        except:
            print("(-) env failed")


def run_KLEE(Programm_name, Programm_name_real, Programm_arguments, duration):
    cmd_argv_1_RUN_KLEE = " " + str(Programm_name)
    cmd_argv_2_RUN_KLEE = str(Programm_name_real)
    cmd_argv_3_RUN_KLEE = str(Programm_arguments)
    print(f"Executing KLEE Script on:\n{Programm_name}\n")

    try:
        subprocess.run(["./Run_Klee.sh", cmd_argv_1_RUN_KLEE, cmd_argv_2_RUN_KLEE, cmd_argv_3_RUN_KLEE, duration], check=True)
    except subprocess.TimeoutExpired:
        print("\nAlternative Harvesting  Process of Commandline Interfaces with ktest-tool")
        KLEE_out_replay_txt_name = cmd_argv_2_RUN_KLEE + "replayAlternativ.txt"
        KLEE_out_extract_cmd = "(ktest-tool klee-last/*.ktest) 2>&1 | grep text: >> " + KLEE_out_replay_txt_name
        print(f"Try ktest-tool on klee-last directory with:\n{KLEE_out_extract_cmd}\n")
        subprocess.run([KLEE_out_extract_cmd], shell=True)
        print(" ")
        print(" Alternativer Ablauf abgeschlossen")
    print ("KLEE Script End")


def extract_KLEE_params():
    extracted_params_klee = list() 
    extracted_params_cumulated = list() 

    InputArgs_KLEE_only= io.StringIO()
    InputArgs_STRING_only= io.StringIO()

    Argslist_KLEE_only= []
    Argslist_STRING_only= []

    replay_file_name_KLEE = "KLEEArgs1.txt"
    STRING_Args_txt_file="Args.txt"
    

    with open(replay_file_name_KLEE, 'r') as Klee_input1:
        InputArgs_KLEE_only= Klee_input1.read()
        for line in Klee_input1:
            InputArgs_KLEE_only = Klee_input1.read()

    print(' ')
    # KLEE PARAMS
    # Argslist_KLEE_only+= re.findall((r'-+\w{1,30}.*\"'),InputArgs_KLEE_only)
    # extracted_params_klee=list(set(Argslist_KLEE_only))
    # extracted_params_klee= [item[:-1] for item in extracted_params_klee]
    Argslist_KLEE_only+= re.findall((r'(\"-+\w{1,30}.*\")'),InputArgs_KLEE_only)
    extracted_params_klee=list(set(Argslist_KLEE_only))
    extracted_params_klee = [item.replace('"', ' ')[1:-1] for item in extracted_params_klee]

    Argslist_KLEE_only+= re.findall((r'(\"-+\w{1,30}.*\")'),InputArgs_KLEE_only)
    extracted_params_klee=list(set(Argslist_KLEE_only))
    #extracted_params_klee= [item[:-1] for item in extracted_params_klee]
    extracted_params_klee = [item.replace('"', ' ')[1:-1] for item in extracted_params_klee]

    
    with open(STRING_Args_txt_file, 'r') as Klee_input_String:
        InputArgs_STRING_only= Klee_input_String.read()
        for line in Klee_input_String:
            InputArgs_STRING_only = Klee_input_String.read()

    # STRINGS PARAMS
    Argslist_STRING_only+=  re.findall((r'-{1,2}\w{1,20}'),InputArgs_STRING_only)
    Argslist_STRING_only+=  re.findall((r'-{1,2}\w{1,20}-{1,2}\w{1,20}'),InputArgs_STRING_only)
    Argslist_STRING_only+=  re.findall((r'-{0,2}\w{1,20}-{1,2}\w{1,20}-{1,2}\w{1,20}'),InputArgs_STRING_only)
    extracted_params_cumulated+=list(set(Argslist_STRING_only))
    #Write KLEE Input Fuzz Corpus
    Myfile=open('fuzzin.txt','w')
    for element in extracted_params_klee:
        Myfile.write(element)
        Myfile.write('\n')
    Myfile.close()
    # Write String Param Corpus

    extracted_params_cumulated += extracted_params_klee
    Myfile1=open('fuzzin_string.txt','w')
    for element in extracted_params_cumulated:
        Myfile1.write(element)
        Myfile1.write('\n')
    Myfile1.close()

    return (extracted_params_klee, extracted_params_cumulated)


print(" [+] Preparing KLEE environment")
prepare_KLEE_env()

print(" [+] Executing KLEE")

threading.Thread(target=run_KLEE(KLEE_Target_Programm, binary, prog_args, duration))

print(" [+] Finished Execution")

print(" [+] Extracting gather parameters")

result_params = extract_KLEE_params()

print(f"Exported KLEE Args:\n{result_params[0]}")

print(f"Exported STRING Args:\n{result_params[1]}\n")