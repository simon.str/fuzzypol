import os 
import logging 
import subprocess
import random 
import string
import re

def extract_values_from_program_source():
    pass


def assign_existing_file_path(param: str, fuzzing_root_file_path: str):
    include_files = list() 

    for (dirpath, dirnames, filenames) in os.walk(fuzzing_root_file_path): 
        for file in filenames: 
            include_files.append(os.path.join(dirpath, file))

    return param + random.choice(include_files)


def assign_random_file_path(param: str):
    rand_file_path = "./"
    for i in range(5): 
        rand_file_path += random.choice(string.ascii_letters) 

    return param + rand_file_path


def assign_values_to_params(value_list: list, params: list, 
        fuzzing_root_file_path: str):
    ''' Assign all parameters with equal sign all values from the value list. 
    '''

    logging.debug("Starting assigning value to parameter with equal sign")
    equal_sign_params = list() 

    with open(fuzzing_root_file_path, 'r') as fd: 
        fuzzing_root_location = fd.read().split

    for param in params.copy(): 
        if '=' in param: 
            params.remove(param)
            equal_sign_params.append(param)

    for param in equal_sign_params: 
        if "FILE" in param: 
            params.append(assign_existing_file_path(param, 
                fuzzing_root_location))
            params.append(assign_random_file_path(random))
        elif "NUM" in param: 
            number_to_be_added = random.randint(1,150)
            params.append(param + str(number_to_be_added))
        else:
            for value in value_list: 
                value_assigned_param = param + value 
                logging.debug(f"Adding value assigned param " 
                    + f"to parameter list: {value_assigned_param}")
                params.append(value_assigned_param)

    return params 


def extract_params_from_bin_string_output(bin_path: str): 
    ''' Method calls utility strings on binary and then extracts all arguments
    '''
    logging.info("Extracting parameters from the binary"
        + " with help of the strings utility")
    tmp_params_list = list() 
    params = list() 

    if not os.path.exists(bin_path):
        logging.error("Binary path does not exist")
        return 

    strings_util_out = subprocess.check_output(["strings", bin_path]).decode()
    strings_util_out = strings_util_out.split('\n') 

    for item in strings_util_out:
        item = item.strip() 
        if len(item) == 0: 
            continue

        if item[0] == "-":
            tmp_params_list.append(item)

    for item in tmp_params_list:
        subitems = item.split() 
        if len(subitems[0]) <=1:
            continue
        if len(subitems)==1: 
            logging.debug(f"Found trivial flag: {subitems[0]}")
            params.append(subitems[0])
        elif len(subitems) == 2: 
            if "=" not in subitems[1]: 
                logging.debug(f"Found trivial flag:{subitems[1]}")
                params.append(subitems[1])
        elif "=" not in subitems[0]:
            logging.debug(f"Found another flag: {subitems[0]}")
            params.append(subitems[0])
        else: 
            # Check if condition is enclosed in square brackets       
            tmp = subitems[0].replace('[', '')
            tmp = tmp.replace(']', '')
            index_equal_sign = tmp.find('=')
            tmp = tmp[:index_equal_sign+1]

            logging.debug(f"Found equal sign flag: {tmp}")
            params.append(tmp)

    return params

def extract_string_source(source_file: str, bin_path: str, param_list):
    ret_list = list() 

    logging.info("Extracting possible value strings for binary")

    with open(source_file, 'r') as source_fd:
        program_source = source_fd.read() 
    
    result = re.findall(r'\"[\w-]*\"', program_source, re.DOTALL)

    strings_util_out = subprocess.check_output(["strings", bin_path]).decode()

    if result: 
        for item in result:
            item = item.replace('"', "")
            if item == "": 
                continue
            if len(item) <= 1:
                continue
            if ((item in strings_util_out)
                    and (("-" + item) not in param_list)
                    and (("--" + item) not in param_list)
                    and (item[0] != "-")):
                logging.debug(f"[+] Found value item: {item}")
                ret_list.append(item)

    return ret_list 


def combine_different_argument_lists(parameter_list: list, arg_finder_file: str): 
    with open(arg_finder_file, 'r') as arg_finder_fd:
        current_results = arg_finder_fd.read()
        current_results = current_results.split('\n')  

    logging.info("Combining results from string output and arg finder")

    for item in parameter_list:
        if item not in current_results:
            logging.debug(f"Adding new parameter to results: {item}")
            current_results.append(item)
    
    print(current_results)

    with open(arg_finder_file, 'w') as arg_finder_fd:
        for item in current_results:
            cleaned_up_item = item.replace(",", "")
            arg_finder_fd.write(cleaned_up_item+"\n")


if __name__=="__main__": 
    # value_list = ["always", "never", "default"]
    logging.basicConfig(level=logging.DEBUG)
    bin_name = "/home/vagrant/examples/afl_instructed/ls"
    fuzzing_root_location = "/synced_folder/sandboxpol/ebpf/custom_mutator/rootlocations.txt"
    result_list = extract_params_from_bin_string_output(bin_name) 
    value_list = extract_string_source("/home/vagrant/examples/src/coreutils-9.0/src/ls.c", bin_name, result_list)
    result_list = assign_values_to_params(value_list, result_list, fuzzing_root_location)
    arg_finder_result_file = "/synced_folder/sandboxpol/ebpf/fuzzzing_with_ebpf/arg_finder/fuzzin_string.txt"
    #combine_different_argument_lists(result_list, arg_finder_result_file)
    
