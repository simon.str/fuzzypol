import os 
import sys
import subprocess
import json 
import BitVector
import time

parent_dir = os.path.abspath('.')
sys.path.insert(1, parent_dir)
#import syscall_validity_checker
#import policiesGenerator
from syscall_finder import extract_bitvector


class SyscallHarvester: 
    def __init__(self, bin_name: str, flag_file: str, uninstrumented_bin: str, debug: bool =False):
        self.flag_file = flag_file
        self.bin_name = bin_name
        self.flags = list()
        self.uninstrumented_bin = uninstrumented_bin
        self.syscall_bitvector = BitVector.BitVector(size=480) 

        for i in range(480):
            self.syscall_bitvector[i]= 0 

        if debug == True:  
            self.debug = subprocess.DEVNULL
        else:
            self.debug = subprocess.STDOUT
        
        if not os.path.isfile(self.flag_file):
            print('No valid argument file provided!')
            exit(1)
        
        self.flags = open(self.flag_file, 'r')
        
    def harvest_syscalls(self):
        print(f"\n\nStarting gathering syscalls for {self.bin_name}\n")
        i = 0; 
        syscall_list = dict()
        failed_flags = list() 

        #create file for first run to negotiate timing problems 
        open(f'./{os.path.basename(self.bin_name)}_syscalls.json', 'w')

        for line in self.flags.readlines(): 
            flag = line

            flag = flag.strip() 
            if flag == '': 
                continue

            i += 1 
            if str.isprintable(flag): 
                print(f"[{i}] Execution of Syscall-Finder\tFlag = {flag}")
            else:
                print(f"[{i}] Execution of Syscall-Finder with non printable arg of order: {[ord(item) for item in flag]}")

            # to prevent the following error message
            # auditd.service: Failed with result 'start-limit-hit'.

            if subprocess.call(['sudo', 'python3', './syscall_finder/main.py', '-k', '-d', '50000', f"{self.bin_name}", flag], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL) != 0: 
                subprocess.call(['sudo', 'systemctl', 'reset-failed', 'auditd'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                if subprocess.call(['sudo', 'python3', './/syscall_finder/main.py', '-k', '-d', '50000', f"{self.bin_name}", flag],  stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL) != 0: 
                    failed_flags.append(flag)    
                

            # Prevent accessing non existent result file path
            while not os.path.exists(f'./{os.path.basename(self.bin_name)}_syscalls.json'): 
                time.sleep(1)

            with open(f'./{os.path.basename(self.bin_name)}_syscalls.json', 'r') as syscalls_json: 
                try:
                    tmp_syscall_list = json.load(syscalls_json) 
                    syscalls_json.close()
                except: 
                    continue 


            if i==1: 
                syscall_list = tmp_syscall_list
                for syscall, attributes in tmp_syscall_list.items(): 
                    if (tmp_syscall_list[syscall][list(attributes.items())[1][0]] == 'allowed'):
                        print(f"\t[+] Adding syscall for architectur x86: {syscall}")
                    if (tmp_syscall_list[syscall][list(attributes.items())[2][0]] == 'allowed'):
                        print(f"\t[+] Adding syscall for architectur x86_64: {syscall}")
                        
                        syscall_number = int(syscall_list[syscall][list(attributes.items())[0][0]])
                        self.syscall_bitvector[syscall_number] = 1
                        #self.analyse_syscall_log_without_wrapper(syscall_number, flag)
            else: 
                for syscall, attributes in tmp_syscall_list.items(): 
                    if (tmp_syscall_list[syscall][list(attributes.items())[1][0]] == 'allowed' and 
                            syscall_list[syscall][list(attributes.items())[1][0]] == 'blocked'):
                        print(f"\t[+] Adding syscall for architectur x86: {syscall}")
                        syscall_list[syscall][list(attributes.items())[1][0]] = 'allowed' 
                    if (tmp_syscall_list[syscall][list(attributes.items())[2][0]] == 'allowed' and 
                            syscall_list[syscall][list(attributes.items())[2][0]] == 'blocked'):
                        print(f"\t[+] Adding syscall for architectur x86_64: {syscall}")
                        syscall_list[syscall][list(attributes.items())[2][0]] = 'allowed'
                        
                        syscall_number = int(syscall_list[syscall][list(attributes.items())[0][0]])
                        self.syscall_bitvector[syscall_number] = 1
                        #self.analyse_syscall_log_without_wrapper(syscall_number, flag)

        return syscall_list

    def transform_syscall_bitvector(self): 
        bitvector = extract_bitvector.BitVectorExtraction(f"./{os.path.basename(self.bin_name)}_syscalls.json", f"./{os.path.basename(self.bin_name)}_syscalls.bits")
        bitvector.read_json_file() 
        bitvector.create_bitvector() 
        bitvector.print_bitvector_to_file() 

        return f"./{os.path.basename(self.bin_name)}_syscalls.bits"


if __name__ == "__main__":  
    syscall_finder_afl_bin = "./fuzzing_dir/ls"
    syscall_finder_args = "./arg_finder/fuzzin_string.txt"
    harvester = SyscallHarvester(syscall_finder_afl_bin, syscall_finder_args, syscall_finder_afl_bin)
    syscalls_dict = harvester.harvest_syscalls()
    harvester.transform_syscall_bitvector()
