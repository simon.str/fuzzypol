import os

import file_access_lists
from file_access_log_analysis import  file_access_constants, file_access_syscall
#import file_access_constants, file_access_syscall


def post_process_file_access_syscall(event):
    result_dict = dict() 
    result_dict["syscall"] = event.syscall 
    result_dict["pid"] = event.pid 
    result_dict["ppid"] = event.ppid 
    result_dict["uid"] = event.uid 
    result_dict["comm"] = event.comm 
    result_dict["ret_val"] = event.ret_val 
    result_dict["dirfd"] = event.dirfd 
    result_dict["pathname"] = event.pathname.decode()
    result_dict["flags"] = event.flags 

    return result_dict

def analyse_file_access_syscall(result_dict): 
    syscall_analysis_function = {   2: file_access_syscall.NonFDSyscall, 
                                    4: file_access_syscall.NonFDSyscall, 
                                    6: file_access_syscall.NonFDSyscall,
                                    21: file_access_syscall.NonFDSyscall,
                                    59: file_access_syscall.NonFDSyscall,
                                    76: file_access_syscall.NonFDSyscall, 
                                    82: file_access_syscall.NonFDSyscall, 
                                    85: file_access_syscall.NonFDSyscall,
                                    86: file_access_syscall.NonFDSyscall, 
                                    87: file_access_syscall.NonFDSyscall,
                                    88: file_access_syscall.NonFDSyscall, 
                                    89: file_access_syscall.NonFDSyscall, 
                                    90: file_access_syscall.NonFDSyscall, 
                                    92: file_access_syscall.NonFDSyscall,
                                    94: file_access_syscall.NonFDSyscall, 
                                    132: file_access_syscall.NonFDSyscall, 
                                    133: file_access_syscall.NonFDSyscall, 
                                    188: file_access_syscall.NonFDSyscall,
                                    189: file_access_syscall.NonFDSyscall, 
                                    191: file_access_syscall.NonFDSyscall,
                                    192: file_access_syscall.NonFDSyscall,
                                    194: file_access_syscall.NonFDSyscall,
                                    195: file_access_syscall.NonFDSyscall,
                                    197: file_access_syscall.NonFDSyscall,
                                    198: file_access_syscall.NonFDSyscall,
                                    235: file_access_syscall.NonFDSyscall,
                                    257: file_access_syscall.FDSyscall, 
                                    259: file_access_syscall.FDSyscall,
                                    260: file_access_syscall.FDSyscall,
                                    262: file_access_syscall.FDSyscall,
                                    263: file_access_syscall.FDSyscall,
                                    264: file_access_syscall.FDSyscall,
                                    265: file_access_syscall.FDSyscall,
                                    266: file_access_syscall.FDSyscall,
                                    267: file_access_syscall.FDSyscall,
                                    268: file_access_syscall.FDSyscall,
                                    269: file_access_syscall.FDSyscall,
                                    316: file_access_syscall.FDSyscall,
                                    322: file_access_syscall.FDSyscall,
                                    332: file_access_syscall.FDSyscall    
                                }
    syscall_obj = syscall_analysis_function[result_dict['syscall']](result_dict)
    syscall_obj.process_file_access() 


def analyse_file_access_queue(event_list:list): 
    for event in event_list: 
        analyse_file_access_syscall(event)

def post_process_pid_generation_syscall(event)->dict:
    result_dict = dict()

    result_dict["syscall"] = event.syscall
    result_dict["pid"] = event.pid 
    result_dict["ppid"] = event.ppid 
    result_dict["ret_val"] = event.ret_val
    result_dict["comm"] = event.comm.decode("utf-8")

    return result_dict


def analyse_pid_generation_syscall(result_dict: dict)->None:
    if ((result_dict['comm'] == "diff" or
            int(result_dict['pid']) in file_access_lists.pid_list) and 
            int(result_dict['ret_val']) > 0): 
        file_access_lists.pid_list[int(result_dict['ret_val'])] = dict()

    # We suppose 1000 tracked process should 
    # be enought for most execution examples
    # Border should be parameterized to customize it for long running examples 
    pid_list_len = len(file_access_lists.pid_list) 
    if pid_list_len >= 1000: 
        remove_last_pids(pid_list_len - 1000)


def remove_last_pids(n: int) -> list:
    ''' Get a list of the lowest n keys in the pid_list. 
    '''

    for i in range(n): 
        file_access_lists.pid_list.pop(min(file_access_lists.pid_list))


def analyse_pid(result_dict, comm_name): 
    if int(result_dict['pid']) in file_access_lists.pid_list: 
        return 1 
    elif result_dict['comm'].decode('UTF-8') == comm_name:
        file_access_lists.pid_list[int(result_dict['pid'])] = dict() 
        return 1 
    else: 
        return 0 