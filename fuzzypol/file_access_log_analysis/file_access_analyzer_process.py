#!/usr/bin/python3

import argparse
import configparser
import multiprocessing
import signal 
import logging
from sqlite3 import Timestamp
import bcc
import os 
import json 
from multiprocessing import shared_memory
import numpy as np 

#import analyse_file_access_log
#import file_access_lists

from file_access_log_analysis import file_access_lists, analyse_file_access_log

bpf_object: bcc.BPF 
perf_event_array_name: str
config: configparser.ConfigParser
interrupt = True
pid_shm: shared_memory.SharedMemory
global_pid_list: np.ndarray
binary_basename: str


def signal_handler(signal, frame): 
    global interrupt 

    interrupt = False 

signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)

def build_PID_filter(program_text:str) -> str:
    ''' Build the filter text to replace the magic string "PID_FILTER" in the
        ebf program loaded.

        Parameters
        --------
        program_text: str
            Current program text
        
        Returns
        ------
        str
            Update program text with replaced magic string
    '''
    pid_filter_string = "if ("
    pids_noisy_programs = get_pids() 

    if not pids_noisy_programs: 
        program_text = program_text.replace("PID_FILTER", "") 
        return program_text
    else: 
        for item in pids_noisy_programs: 
            pid_filter_string += f"(pid == {item}) || "

    pid_filter_string = pid_filter_string[:-4]
    pid_filter_string += ") return 0;" 
    program_text = program_text.replace("PID_FILTER", pid_filter_string)

    return program_text

def post_process_file_access_syscall(event:bcc.table)->dict:
    '''Transform an event of type bcc.table to a Python dictionary.
    '''
    result_dict = dict() 
    result_dict["syscall"] = event.syscall 
    result_dict["pid"] = event.pid 
    result_dict["ppid"] = event.ppid 
    result_dict["uid"] = event.uid 
    result_dict["comm"] = event.comm 
    result_dict["ret_val"] = event.ret_val 
    result_dict["dirfd"] = event.dirfd 

    # In case of splicing non-decodable chars are given     
    try: 
        result_dict["pathname"] = event.pathname.decode()
    except UnicodeError: 
        # Return none as pathname since the filename cannot be valid.
        result_dict["pathname"] = None

    result_dict["flags"] = event.flags 

    return result_dict

def get_pids()-> list:
    '''Returns a list of all process PIDs started before Fuzzing.

    Basically perform a lookup of all process PIDs currently listed in "/proc"
    '''
    proc_dirs = os.listdir("/proc")
    actual_process_dirs = list() 
    for item in proc_dirs: 
        try: 
            actual_process_dirs.append(int(item))
        except ValueError:
            continue
    
    actual_process_dirs.remove(os.getpid())

    return actual_process_dirs 


def init_pid_generation_shm():
    ''' Attach to memory shared by pid generation analyzer
    '''
    global pid_shm
    global global_pid_list 
    
    array_size = np.zeros(1000, dtype=np.int64).nbytes
    try:    
        pid_shm = shared_memory.SharedMemory(name="pid_shm")
    except FileNotFoundError:
        logging.error("Could not initiliaze shared memory")
        exit(-1) 
    global_pid_list = np.ndarray(1000, dtype=np.int64, buffer=pid_shm.buf)
    logging.debug("File Access Analysis Process: "
        + "Attached to shared memory")


def close_shared_memory(): 
    global pid_shm
    global global_pid_list

    del global_pid_list
    pid_shm.close()
    logging.debug("File Access Analysis Process")


def init_BPF(config: configparser.ConfigParser):
    '''Initialize the BPF object. 

    Read and transform the BPF program text. All magic string will be replaced 
    accordingly. 
    '''
    global bpf_object
    
    with (
        open(
            "/synced_folder/sandboxpol/ebpf/"+
            "ebpf_filter_programs/file_access_bpf.c", 
            "r") as program_src_file
        ): 
        program_text = program_src_file.read() 

    if config.getboolean('EBPF_MAGIC_STRINGS', 'pid_filter'): 
        program_text=build_PID_filter(program_text)
        logging.debug("Applied PID filtering")
    else: 
        program_text.replace('PID_FILTER', '')
        logging.debug("PID Filter not applied")

    
    program_text = program_text.replace("UID_FILTER",
            f"if (uid != {config['EBPF_MAGIC_STRINGS']['uid']}) {{return 0;}}")

    cflag_list = list()
    if config.getboolean('DEBUG_PARAMETERS', 
            'disable_macro_redefinition_warning'):
        cflag_list.append("-Wno-macro-redefined")
        logging.debug("Disabled macro warnings")
    
    debug = 0
    if config.getboolean('DEBUG_PARAMETERS', 'debug_preprocessor'):
        debug = bcc.DEBUG_PREPROCESSOR
        logging.debug("Printed Debugging Preprocessor Output")

        
    bpf_object = bcc.BPF(text=program_text, cflags=cflag_list, debug=debug)
    logging.info("Successfully initialized according BPF object")



def analyze_file_access_event(cpu, data, size):
    ''' Callback function to handle new event 
        from the specified perf event array.
    '''
    global bpf_object 

    event = bpf_object[perf_event_array_name].event(data) 

    perf_event = post_process_file_access_syscall(event)

    # Syscall 80 (CHDIR) to consider AT_FDCWD
    if perf_event["syscall"] == 80:
        logging.debug("Syscall Analyzer: Found chdir command -> Changing dir")
        if  not perf_event["ret_val"] == 0 : 
            return 
        if  perf_event['pid'] not in file_access_lists.pid_list: 
            file_access_lists.pid_list[perf_event['pid']] = dict()
        file_access_lists.pid_list[perf_event['pid']]['AT_FDCWD'] = perf_event["pathname"] 
        return 

    # Case 1: PID alread tracked in local PID list -> Proceed with analysing
    if perf_event['pid'] in file_access_lists.pid_list:
        pass
    # Case 2: PID not tracked but comm matches expectations 
    # -> Add PID and continue analysing
    elif ((perf_event['uid'] == int(config['EBPF_MAGIC_STRINGS']['uid'])) 
            and (perf_event['pid'] not in file_access_lists.pid_list)
            and (perf_event['comm'].decode("utf-8")) == binary_basename):
        file_access_lists.pid_list[perf_event['pid']] = dict()
    # Case 3: PID not locally tracked but in global PID storage:
    # -> Add PID to local storage and continue computing
    elif (perf_event['pid'] in global_pid_list): 
        file_access_lists.pid_list[perf_event['pid']] = dict() 
    else: 
        return

    logging.debug(f"Received Event: {perf_event}")
    
    analyse_file_access_log.analyse_file_access_syscall(perf_event)


def dump_results(): 
    ''' Dump Results into json file to config file specified location. 
    '''
    logging.debug("Finished execution of process with perf event array "
            + f"{perf_event_array_name} with the following results:"
            + f"\n\tRead= {file_access_lists.file_access_read}"
            + f"\n\tWrite= {file_access_lists.file_access_write}"
            + f"\n\tExecute= {file_access_lists.file_access_execute}"
            + f"\n\tPID List= {file_access_lists.pid_list}")

    file_path = os.path.join(config['RESULTS']['file_paths'], 
                    (perf_event_array_name + ".json"))
    concattenated_results = dict() 
    concattenated_results['read'] = file_access_lists.file_access_read
    concattenated_results['write'] = file_access_lists.file_access_write
    concattenated_results['execute'] = file_access_lists.file_access_execute

    with open(file_path, 'w') as fd_file_path: 
        json.dump(concattenated_results, fd_file_path, indent=6)
    


def file_access_analyzer_process_main(ringbuffer_name: str, 
        config_file_path: str,
        event: multiprocessing.Event):
    global bpf_object
    global perf_event_array_name
    global config 
    global binary_basename

    init_pid_generation_shm()

    perf_event_array_name = ringbuffer_name

    config = configparser.ConfigParser() 
    config.read(config_file_path)

    binary_basename = config["EBPF_MAGIC_STRINGS"]["binary_basename"]

    init_BPF(config)

    event.set() 
    bpf_object[ringbuffer_name].open_perf_buffer(analyze_file_access_event, page_cnt=256)

    # Proceed to pull data from the perf ring buffer until SIGTERM or SIGINT
    # are received
    while interrupt:
        try:
            bpf_object.perf_buffer_poll()
        except KeyboardInterrupt:
            break;
        
            
    
    logging.debug("File Access Analysis Process: ")
    close_shared_memory()
    dump_results()
    logging.info("File Access Analysis Process: "
        + "Successfully ended execution and done cleanup.")

if __name__ == "__main__": 
    # viable_ringbuffer_names = ["file_access_events_queue1", 
    #                             "file_access_events_queue2"]

    # logging.basicConfig(level=logging.DEBUG,
    #     format='%(filename)s %(funcName)s: %(message)s')    
    # parser = argparse.ArgumentParser(description="Standalone Process to "\
    #     "simultaneously process file access")

    # parser.add_argument('-n', '--name', required=True, dest="ringbuffer_name",
    #     help="Name of the ringbuffer to analyze.", 
    #     choices=viable_ringbuffer_names)
    # parser.add_argument('-d', '--duration', default="30", dest="duration", 
    #     help="Duration before poll requests to time out")
    # parser.add_argument('-c' ,'--config', 
    #     default="/synced_folder/sandboxpol/ebpf/fuzzzing_with_ebpf"
    #     +"/file_access_log_analysis/file_access_analyzer_config.ini", 
    #     dest="config_file_path", 
    #     help="Configuration file for this analyzer module")

    # args = parser.parse_args()
    
    # logging.debug(f"Args:\tRingbuffer Name= {args.ringbuffer_name}"\
    #     f"\tDuration= {args.duration}sec\tConfig= {args.config_file_path}")

    # file_access_analyzer_process_main(args.ringbuffer_name, 
    #    args.config_file_path) 
    pass

