import configparser
import os
import file_access_lists 
import re 
import logging 


def remove_obscure_syntax(file_access_list:list) -> list: 
    ''' Removes obscure syntax, such as */./* pattern
        that are created during path joing 
    '''
    for file_path in file_access_list.copy(): 
        # Relativ
        if re.search(r".*\/\.\/.*", file_path) is not None:
            try: 
                file_access_list.remove(file_path)
                replaced_file = re.sub(r"\/\.\/", "/", file_path)
                file_access_list.append(replaced_file)
                continue
            except ValueError: 
                logging.warning(f"Tried to replace {file_path} and could " 
                    + f"not lookup the according item. (ValueError)"
                    + f"file_path = {file_path}\nfile_accss_list= {file_access_list}\n")
        # Relativ file path: Current Dir
        if re.search(r".*\/\.$", file_path):
            try:
                file_access_list.remove(file_path)
                replaced_file = re.sub(r"\/\.$", "", file_path)
                file_access_list.append(replaced_file)
                continue
            except ValueError: 
                logging.warning(f"Tried to replace {file_path} and could " 
                    + f"not lookup the according item. (ValueError)")
        # Relativ file path: Parent Dir
        if re.search(r".*\/\.\.$", file_path):
            try:
                file_access_list.remove(file_path)
                replaced_file = re.sub(r"\/\.\.$", "", file_path)
                file_access_list.append(os.path.split(replaced_file)[0])
                continue
            except ValueError: 
                logging.warning(f"Tried to replace {file_path} and could " 
                    + f"not lookup the according item. (ValueError)")

    return file_access_list


def post_process_file_access(config:configparser.ConfigParser): 
    file_access_lists.file_access_read = generic_post_process(
        file_access_lists.file_access_read, config, "read")
    file_access_lists.file_access_read = list(dict.fromkeys(
        file_access_lists.file_access_read))
    
    file_access_lists.file_access_write = generic_post_process(
        file_access_lists.file_access_write, config, "write")
    file_access_lists.file_access_write = list(dict.fromkeys(
        file_access_lists.file_access_write))

    file_access_lists.file_access_execute = generic_post_process(
        file_access_lists.file_access_execute, config, "execute")
    file_access_lists.file_access_execute =  list(dict.fromkeys(
        file_access_lists.file_access_execute))


def generic_post_process(file_access_list:list, 
        config:configparser.ConfigParser, mode: str) -> list: 
    ''' Generically removes all file occurences, which can be found 
        in the Custom Mutator Rootlocation or the CWD. 

        Returns the new list. 
    ''' 
    rootlocation_custom_mutator = config['AFL']['rootlocation_custom_mutator']

    file_access_lists.file_access_removed[mode] = list()

    file_access_list = remove_obscure_syntax(file_access_list)

    with open(rootlocation_custom_mutator, 'r') as rootlocation_fd: 
        rootlocation = rootlocation_fd.read()

    rootlocation_files = filename_walk(rootlocation.strip()) 
    cwd_files = filename_walk(os.getcwd())

    for item in file_access_list.copy(): 
        if (item in rootlocation_files) or (item in cwd_files):
            file_access_lists.file_access_removed[mode].append(item)
            file_access_list.remove(item)

    return file_access_list


def filename_walk(enum_root:str)->list: 
    '''Recursively list all files in location dirpath
    '''

    result_list = list() 

    for dirpath in os.walk(enum_root): 
        for dirname in dirpath[1]: 
            result_list.append(os.path.join(dirpath[0], dirname))

        for filename in dirpath[2]: 
            result_list.append(os.path.join(dirpath[0], filename))
        
    return result_list 


if __name__=="__main__": 
    generic_post_process("/something")