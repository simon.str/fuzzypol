import os 
import logging
import sys
import re 
#from bcc import BPF 
import bcc 
import io 
from BitVector import BitVector
import subprocess 
import configparser

filter_src_file: str
filter_path: str
standalone_preloader: str
seccomp_flag: bool

seccomp_action = {0x80000000: "SECCOMP_RET_KILL_PROCESS",
                  0x00000000: "SECCOMP_RET_KILL_THREAD",
                  0x00000000: "SECCOMP_RET_KILL",
                  0x00030000: "SECCOMP_RET_TRAP",
                  0x00050000: "SECCOMP_RET_ERRNO",
                  0x7fc00000: "SECCOMP_RET_USER_NOTIF",
                  0x7ff00000: "SECCOMP_RET_TRACE",
                  0x7ffc0000: "SECCOMP_RET_LOG",
                  0x7fff0000: "SECCOMP_RET_ALLOW"}


def adjust_globals_fp_checker(config: configparser.ConfigParser): 
    global filter_src_file
    global filter_path 
    global standalone_preloader

    filter_src_file = config['BPF']['seccomp_bpf']
    filter_path = config['RESULTS']['syscall_bitvector_file']
    standalone_preloader = config['FPCHECKER']['standalone_preloader']

def post_process_seccomp_events(event):
    global seccomp_action
    result_dict = dict()

    result_dict["syscall"] = event.syscall
    result_dict["pid"] = event.pid
    result_dict["ppid"] = event.ppid
    result_dict["uid"] = event.uid
    result_dict["seccomp_action"] = seccomp_action[abs(event.seccomp_action)]
    result_dict["comm"] = event.comm.decode("utf-8")

    return result_dict


def adjust_seccomp_filter(result_dict):
    global filter_path
    global seccomp_flag

    with open(filter_path, 'r') as syscall_filter_bitmask_stream:
        bitmask = io.StringIO(
            syscall_filter_bitmask_stream.read().replace('\n', ''))
        syscall_bitmask = BitVector(fp=bitmask)

    # When internal SECCOMP filters are used this Path is going to be used
    if syscall_bitmask[int(result_dict['syscall'])] == 1: 
        seccomp_flag=False
        return

    logging.info(
        f"Found Syscall {result_dict['syscall']} in False Positive Tests")
    
    syscall_bitmask[int(result_dict['syscall'])] = 1

    with open(filter_path, 'w') as adjusted_syscall_filter_stream:
        adjusted_syscall_filter_stream.write(str(syscall_bitmask))


def seccomp_message_processing(cpu, data, size):
    global seccomp_flag
    global seccomp_action
    global b
    result_dict = dict()
    event = b["events"].event(data)

    result_dict = post_process_seccomp_events(event)

    if (result_dict["seccomp_action"] != "SECCOMP_RET_KILL_PROCESS" and 
        result_dict["seccomp_action"] != "SECCOMP_RET_KILL_THREAD"):
        logging.info("Successfully found a syscall baseline for the SUT")
        seccomp_flag = False
    else:
        if int(result_dict['syscall']) == 30 or int(result_dict['syscall'] == 31):
            logging.debug(f"Standalone Execution: Found necessary syscall {result_dict['syscall']}")
            seccomp_flag = True
            adjust_seccomp_filter(result_dict)
        else: 
            logging.debug(f"Found another not yet tracked syscall:{result_dict['syscall']}")
            seccomp_flag = True
            adjust_seccomp_filter(result_dict)  


def event_loop(sut, cli):
    global seccomp_flag
    global b

    b["events"].open_perf_buffer(seccomp_message_processing)
    os.environ["PG_BITVECTOR_FILE"] = filter_path 
    os.environ["LD_PRELOAD"] = standalone_preloader
    while seccomp_flag:
        try:
            if logging.root.level > logging.DEBUG:
                # p = subprocess.run([sut], input=cli.encode(), 
                #                     env=os.environ, stdout=subprocess.DEVNULL, 
                #                     stderr=subprocess.DEVNULL)
                p = subprocess.run([sut], input=cli, 
                                    env=os.environ, stdout=subprocess.DEVNULL, 
                                    stderr=subprocess.DEVNULL)
            else:
                # p = subprocess.run([sut], input=cli.encode(), env=os.environ)
                # logging.info(f"Executed SUT; Exit Returncode: {p.returncode}")
                p = subprocess.run([sut], input=cli, env=os.environ)
                logging.info(f"Executed SUT; Exit Returncode: {p.returncode}")
            #seccomp_flag = False 
            b.perf_buffer_poll(timeout=500)

            # 159 for bad syscall        
            if (p.returncode != 159 and p.returncode != -31) or seccomp_flag == False:
                break; 
        except KeyboardInterrupt:
            break; 


def initialize_BPF(config: configparser.ConfigParser): 
    global b 
    global filter_src_file 

    cflag_list = list()
    if config.getboolean('DEBUG_PARAMETERS', 
            'disable_macro_redefinition_warning'):
        cflag_list.append("-Wno-macro-redefined")
        logging.debug("Disabled macro warnings")
    
    debug = 0
    if config.getboolean('DEBUG_PARAMETERS', 'debug_preprocessor'):
        debug = bcc.DEBUG_PREPROCESSOR
        logging.debug("Printed Debugging Preprocessor Output")

    b = bcc.BPF(src_file=filter_src_file, cflags=cflag_list, debug=debug)
    b.attach_kprobe(event="seccomp_run_filters", 
                        fn_name="seccomp_run_filters")
    b.attach_kretprobe(event="seccomp_run_filters", 
                        fn_name="ret_seccomp_run_filters")


def get_testcases(test_case_dir:str)-> list: 
    ''' Function to list AFL crashes and remove README. 
    '''
    if not os.path.exists(test_case_dir): 
        logging.error("AFL Crashes Directory does not exist; going to abort")
        sys.exit(-1)

    testcases = [os.path.join(test_case_dir, crash_path) for crash_path in os.listdir(test_case_dir)]

    for crash_path in testcases.copy():
        if re.match(r'.*README\.txt.*', crash_path):
            logging.debug("get_testcases: Going to skip README")
            testcases.remove(crash_path)

    return testcases


def test_single_testcase_for_false_positive(bin_path: str, 
        test_case_file_path: str): 
    with open(test_case_file_path,'rb')as test_case_file:
        input = test_case_file.read() 

    logging.debug(f"Execute Single Test Case with Input:{input}")
    event_loop(bin_path, input)


def check_crashes_for_false_positives(config): 
    global seccomp_flag

    bin_path = config['PREEVALUATION']['bin_path']

    adjust_globals_fp_checker(config)
    initialize_BPF(config) 
    testcase_list = get_testcases("/fuzzing/out/default/crashes")
    
    for testcase_path in testcase_list:
        seccomp_flag = True 
        logging.info(f"Executing testcase {testcase_path}")
        test_single_testcase_for_false_positive(bin_path, testcase_path)


if __name__ == "__main__": 
    logging.basicConfig(level=logging.DEBUG)
    # initialize_BPF() 
    # testcase_list = get_testcases("/fuzzing/out/default/crashes")
    # test_single_testcase_for_false_positive(testcase_list[0])
    #initialize_BPF() 
    #event_loop(".", "./false_positive_tester/ls" )

    check_crashes_for_false_positives() 