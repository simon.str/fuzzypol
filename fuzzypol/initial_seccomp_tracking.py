#!/usr/bin/python3
import bcc
import argparse
import os
import sys
import subprocess
from BitVector import BitVector
import io
import logging
import configparser

# Need to be specified in config file 
filter_src_file = "./seccomp_bpf.c"
seccomp_flag = True

def initialize_BPF(config: configparser.ConfigParser):
    global filter_src_file

    cflag_list = list()
    if config.getboolean('DEBUG_PARAMETERS', 
            'disable_macro_redefinition_warning'):
        cflag_list.append("-Wno-macro-redefined")
        logging.debug("Disabled macro warnings")
    
    debug = 0
    if config.getboolean('DEBUG_PARAMETERS', 'debug_preprocessor'):
        debug = bcc.DEBUG_PREPROCESSOR
        logging.debug("Printed Debugging Preprocessor Output")

    b = bcc.BPF(src_file=filter_src_file, cflags=cflag_list, debug=debug)
    if b.attach_kprobe(event="seccomp_run_filters", 
        fn_name="seccomp_run_filters") == None:
        logging.error("\tCould not attach Seccomp KProbe")
        sys.exit(-1)
    if b.attach_kretprobe(event="seccomp_run_filters", 
        fn_name="ret_seccomp_run_filters") == None:
        logging.error("\tCould not attach Seccomp KRetProbe")
        sys.exit(-1)

    logging.info("Sucessfully initialized Seccomp BPF program")

    return b 


seccomp_action = {0x80000000: "SECCOMP_RET_KILL_PROCESS",
                  0x00000000: "SECCOMP_RET_KILL_THREAD",
                  0x00000000: "SECCOMP_RET_KILL",
                  0x00030000: "SECCOMP_RET_TRAP",
                  0x00050000: "SECCOMP_RET_ERRNO",
                  0x7fc00000: "SECCOMP_RET_USER_NOTIF",
                  0x7ff00000: "SECCOMP_RET_TRACE",
                  0x7ffc0000: "SECCOMP_RET_LOG",
                  0x7fff0000: "SECCOMP_RET_ALLOW"}


def post_process_seccomp_events(event):
    global seccomp_action
    result_dict = dict()

    result_dict["syscall"] = event.syscall
    result_dict["pid"] = event.pid
    result_dict["ppid"] = event.ppid
    result_dict["uid"] = event.uid
    result_dict["seccomp_action"] = seccomp_action[abs(event.seccomp_action)]
    result_dict["comm"] = event.comm.decode("utf-8")

    return result_dict


def adjust_seccomp_filter(result_dict):
    global filter_path

    with open(filter_path, 'r') as syscall_filter_bitmask_stream:
        bitmask = io.StringIO(
            syscall_filter_bitmask_stream.read().replace('\n', ''))
        syscall_bitmask = BitVector(fp=bitmask)

    logging.info(
        f"Found Syscall {result_dict['syscall']} for initial Syscall Baseline")
    syscall_bitmask[int(result_dict['syscall'])] = 1

    with open(filter_path, 'w') as adjusted_syscall_filter_stream:
        adjusted_syscall_filter_stream.write(str(syscall_bitmask))


def seccomp_message_processing(cpu, data, size):
    global seccomp_flag
    global seccomp_action
    global __b__ 
    result_dict = dict()
    event = __b__["events"].event(data)

    result_dict = post_process_seccomp_events(event)

    if (result_dict["seccomp_action"] != "SECCOMP_RET_KILL_PROCESS" and 
        result_dict["seccomp_action"] != "SECCOMP_RET_KILL_THREAD"):
        logging.info("Successfully found a syscall baseline for the SUT")
        seccomp_flag = False
    else:
        logging.debug("Missing SUT syscall is added to the syscall filter")
        adjust_seccomp_filter(result_dict)
        seccomp_flag = True


def event_loop(b, cli, sut):
    global seccomp_flag
    global __b__ 
    __b__ = b 
    b["events"].open_perf_buffer(seccomp_message_processing)
    logging.debug("Entering initial_seccomp_tracking main loop")
    while seccomp_flag:
        try:
            if logging.root.level > logging.DEBUG:
                p = subprocess.run([sut], input=cli.encode(), 
                                    env=os.environ, stdout=subprocess.DEVNULL, 
                                    stderr=subprocess.DEVNULL)
            else:
                p = subprocess.run([sut],
                                   input=cli.encode(), env=os.environ)
            logging.debug(f"Executed SUT; Exit Returncode: {p.returncode}")
            b.perf_buffer_poll(timeout=3000)

            if p.returncode == 0:
                break; 
        except KeyboardInterrupt:
            exit()


def initial_seccomp_tracking_main(bitvector_file, sut, cli, config):
    global filter_path

    filter_path = bitvector_file

    logging.info("Executed initial_seccomp_tracking with the following args:\n"\
        f"\tBitvector-File: {bitvector_file}\n\tSUT: {sut}\n\tCLI-Flag: {cli}")
    os.environ["PG_BITVECTOR_FILE"] = bitvector_file
    os.environ["LD_PRELOAD"] = "./libcombined_preloader.so"

    b = initialize_BPF(config)

    event_loop(b, cli, sut)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    parser = argparse.ArgumentParser(
        description="Create a baseline of syscalls")
    parser.add_argument('-b', dest='pg_bitvector',
                        required=True, help="PG_BITVECTOR_FILE declaration")
    parser.add_argument('-s', dest='sut', required=True,
                        help="SUT declaration")
    parser.add_argument('-cli', dest='cli', required=True, help="CLI flag")

    args = parser.parse_args()

    logging.debug("\tExecuted Initial Seccomp Tracking as Main module")
    initial_seccomp_tracking_main(args.pg_bitvector, args.sut, args.cli)
