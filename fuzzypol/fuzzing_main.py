#!/usr/bin/python3

import argparse
import logging
import BitVector
import io
import subprocess
import os
import bcc
import configparser
import multiprocessing as mp 
from multiprocessing import shared_memory
import numpy as np
import random 
import json 
import signal 
import pyseccomp as seccomp

from py import process

import initial_seccomp_tracking
from arg_finder import symbolic_execution_wrapper,extract_strings
from syscall_finder import harvest_syscalls
from false_positive_tester import false_positiv_test
from file_access_log_analysis import analyse_file_access_log, \
                                        post_process_file_lists, \
                                        file_access_analyzer_process
import file_access_lists                                        
import pid_filter as pf
from utils import testcorpus_generator


seccomp_action = {0x80000000:"SECCOMP_RET_KILL_PROCESS", 
                   0x00000000:"SECCOMP_RET_KILL_THREAD",
                   0x00030000:"SECCOMP_RET_TRAP",
                   0x00050000:"SECCOMP_RET_ERRNO", 
                   0x7fc00000:"SECCOMP_RET_USER_NOTIF",
                   0x7ff00000:"SECCOMP_RET_TRACE",
                   0x7ffc0000:"SECCOMP_RET_LOG",
                   0x7fff0000:"SECCOMP_RET_ALLOW"}


file_access_queue = list() 
file_access_queue_counter  = 0 
file_access_counter = 0 
tmp_last_file_access_pid = -1
pid_shm: shared_memory.SharedMemory


def adjust_config(bin_path: str, config: configparser.ConfigParser): 
    bin_basename = os.path.basename(bin_path)
    cwd = os.getcwd() 

    # TODO: Copy these file to their location 
    config['KLEE']['bin_name'] = bin_basename
    config['KLEE']['bitcode'] = f"{bin_basename}.bc"

    config['PREEVALUATION']['bin_path'] = bin_path

    config['RESULTS']['syscall_bitvector_file'] = f"./{bin_basename}_syscalls.bits"


    with open("./policy_generator.ini", 'w') as configfile:
        config.write(configfile)


    file_access_config_file_path = "./file_access_log_analysis/file_access_analyzer_config.ini"
    config_file_access_analyser = configparser.ConfigParser() 
    config_file_access_analyser.read(file_access_config_file_path) 
    config_file_access_analyser["EBPF_MAGIC_STRINGS"]["binary_basename"] = bin_basename

    with open(file_access_config_file_path, 'w') as file_access_config_fd:
        config_file_access_analyser.write(file_access_config_fd)

def execute_arg_finder(config:configparser.ConfigParser, duration:str = "180"):
    ''' Wrapper function to execute the argument finder during pre evaluation. 

        Parameters
        --------
        config: configparser.ConfigParser
            Config contains the path to the symbolic wrapper script, 
            bin name as well as bicode path. 
        duration: str
            Restricts execution time; string representation of int
        
        Returns
        ------
        str: 
            File path to the result file 
    '''
    cwd = os.getcwd() 
    os.chdir("./arg_finder/")

    symbolic_execution_script = config['KLEE']['symbolic_execution_wrapper']
    bitcode = config['KLEE']['bitcode']
    aflplusplus_executable = config['KLEE']['bin_name']
    cov_dir = config['KLEE']['cov_dir']

    symbol_execution = symbolic_execution_wrapper.SymbolicExecutionWrapper(
        symbolic_execution_script, bitcode, aflplusplus_executable, "5", 
        cov_dir, duration)

    symbol_execution.execute()
    symbol_execution.process_output()
    #symbol_execution.clear()
    result_file = symbol_execution.get_result_file_name()
    os.chdir(cwd)

    return result_file


def afl_runner_function(bin_path: str, fuzzing_duration: str, 
        timeout_mode:bool =True, attach_input:str =""): 
    ''' Wrapper function to execute AFL in a new process concurrently.

        This function also changes its UID/GID since all UID filter rely on 
        a changed UID. Further, the GID is changed to 1000 to be able to access
        AFLs fuzzing dir. 

        If the logging level is higher than INFO the complete standart output 
        is going to be hidden.  

        Parameters
        -----------
        bin_path: str
            Path to the Auditee 
        fuzzing_duration: str
            Restricts execution time; string representation of int
    '''
    global afl_input_dir
    global afl_output_dir
    global syscall_bitvector_file
    global afl_python_module
    global afl_python_path

    
    testcorpus_generator.generate_test_copora("./arg_finder/fuzzin_string.txt",
        afl_input_dir, attach_input)

    os.environ["PG_BITVECTOR_FILE"] = syscall_bitvector_file

    if timeout_mode: 
        os.environ["AFL_PRELOAD"]= "./libtimeout_combined_preloader.so"
        os.environ["AFL_HANG_TMOUT"] = 1100
    else: 
        os.environ["AFL_PRELOAD"]= "./libcombined_preloader.so"
    
    #os.environ["AFL_PRELOAD"]= "./libseccomp_preloader.so"

    # os.environ["PYTHONPATH"] = afl_python_path
    # os.environ["AFL_PYTHON_MODULE"] = afl_python_module

    # Enable to not suppress Binary STDOUT     
    # os.environ["AFL_DEBUG_CHILD"] = "1"

    # Set GID accordingly to access Custom Mutator files 
    os.setgid(1000)
    os.setuid(2000)

    logging.info(f"Started AFL Fuzzing Process:" \
            f"\t{bin_path} fuzzing duration={fuzzing_duration}")

    if logging.root.level > logging.INFO:
        subprocess.run(["afl-fuzz", "-i", afl_input_dir, "-o", afl_output_dir, 
                        "-V", fuzzing_duration, bin_path], 
                        stdout=subprocess.DEVNULL)    
    else:
        subprocess.run(["afl-fuzz", "-i", afl_input_dir, "-o", afl_output_dir, 
                        "-V", fuzzing_duration, bin_path])


def initialize_BPF(config:configparser.ConfigParser): 
    ''' Initialize the eBPF program for seccomp tracking.

        Parameters
        ----------
        config: configparser.ConfigParser
            UID for the according UID Filter is specified there 
    '''
    global b 
    global filter_src_file 


    cflag_list = list()
    if config.getboolean('DEBUG_PARAMETERS', 
            'disable_macro_redefinition_warning'):
        cflag_list.append("-Wno-macro-redefined")
        logging.debug("Disabled macro warnings")
    
    debug = 0
    if config.getboolean('DEBUG_PARAMETERS', 'debug_preprocessor'):
        debug = bcc.DEBUG_PREPROCESSOR
        logging.debug("Printed Debugging Preprocessor Output")


    with open(filter_src_file, 'r') as ebpf_filter: 
        ebpf_filter_text = ebpf_filter.read() 

    ebpf_filter_text = pf.build_PID_filter(ebpf_filter_text)
    # TODO: Replace magic string for UID with config parameter
    ebpf_filter_text = ebpf_filter_text.replace("UID_FILTER",
        f'if (uid != {2000}) {{return 0;}}')
    b = bcc.BPF(text=ebpf_filter_text, cflags=cflag_list, debug=debug)
    b.attach_kprobe(event="seccomp_run_filters", 
                        fn_name="seccomp_run_filters")
    b.attach_kretprobe(event="seccomp_run_filters", 
                        fn_name="ret_seccomp_run_filters")

    
def post_process_seccomp_events(event)-> dict:
    ''' Extract the event data of seccomp events and return an according 
        dictionary. 

        Available entries:
            result_dict["syscall"]\n
            result_dict["pid"]\n
            result_dict["ppid"]\n
            result_dict["uid"]\n
            result_dict["seccomp_action"]\n
            result_dict["comm"]\n
    '''
    global seccomp_action
    result_dict = dict()

    result_dict["syscall"] = event.syscall
    result_dict["pid"] = event.pid
    result_dict["ppid"] = event.ppid
    result_dict["uid"] = event.uid
    # Some seccomp return values do invoke a error return code thus making 
    # it unpredictable to cover 
    try: 
        result_dict["seccomp_action"] = seccomp_action[abs(event.seccomp_action)]
    except KeyError: 
        result_dict["seccomp_action"] = "SECCOMP_RET_ALLOW"
    result_dict["comm"] = event.comm.decode("utf-8")

    return result_dict


def seccomp_handler(cpu, data, size):
    ''' Callback function for pulling events through the ring buffer. 
    '''
    global b 
    event = b["events"].event(data)
    result_dict = post_process_seccomp_events(event)

    if (result_dict['seccomp_action'] == "SECCOMP_RET_KILL_PROCESS" or
            result_dict['seccomp_action'] == "SECCOMP_RET_KILL_THREAD"):
        
        adjust_syscall_bitvector_file(int(result_dict['syscall']))


def adjust_syscall_bitvector_file(syscall_index: int):
    ''' Function to alter the existing bitvector and adjust one position:

        Change the specified syscall index to 1.
    '''
    global syscall_bitvector_file

    with open(syscall_bitvector_file, 'r') as syscall_filter_bitmask_stream:
        bitmask = io.StringIO(syscall_filter_bitmask_stream.read().replace('\n'
        , ''))
        syscall_bitmask = BitVector.BitVector(fp=bitmask)

    syscall_name = seccomp.resolve_syscall(seccomp.Arch.X86_64, syscall_index).decode()
    logging.info(f"\t[i] Adjusting Bitvector for syscall {syscall_name} ({syscall_index}).")
    syscall_bitmask[syscall_index] = 1

    with open(syscall_bitvector_file, 'w') as adjusted_syscall_filter_stream:
        adjusted_syscall_filter_stream.write(str(syscall_bitmask))


def adjust_fuzzing_input_file(arg_input: str):
    ''' Take over the input provided by the argument vector 
        to the input corpus.
    '''
    global afl_input_dir 

    with (open(os.path.join(afl_input_dir, "input.txt"), 'w') 
        as initial_test_seed):
        initial_test_seed.write(arg_input + '\n')


def find_syscalls(config:configparser.ConfigParser):
    ''' Wrapper script for executing the original syscall finder one time 
        upfront to get syscalls needed in constructors and program setup. 
    '''
    syscall_finder_afl_bin = config['PREEVALUATION']['bin_path']
    syscall_finder_args = config['PREEVALUATION']['args']
    harvester = harvest_syscalls.SyscallHarvester(syscall_finder_afl_bin, 
        syscall_finder_args, syscall_finder_afl_bin)
    harvester.harvest_syscalls()
    harvester.transform_syscall_bitvector()


def adjust_globals(config: configparser.ConfigParser):
    ''' Method for altering the config file to the user supplied input.
    '''
    global afl_python_path 
    global afl_python_module
    global afl_input_dir 
    global afl_output_dir
    global syscall_bitvector_file
    global filter_src_file 

    afl_python_path = config['AFL']['afl_python_path']
    afl_python_module = config['AFL']['afl_python_module']
    afl_input_dir = config['AFL']['afl_input_dir']
    afl_output_dir= config['AFL']['afl_output_dir']

    filter_src_file = config['BPF']['seccomp_bpf']

    syscall_bitvector_file = config['RESULTS']['syscall_bitvector_file']


def init_file_access_processes()-> list: 
    ''' Automaticall set up processes for analyzing file access. 

        This function is going to wait until the subprocesses have finished 
        initialization for the bpf object and only return afterwards. 

        Return
        ------
        list: List containing the started processes 
    '''
    event_1 = mp.Event() 
    event_2 = mp.Event() 

    config_file_path = "/synced_folder/sandboxpol/ebpf/fuzzzing_with_ebpf" \
        +"/file_access_log_analysis/file_access_analyzer_config.ini"

    process_1 = mp.Process(
        target=file_access_analyzer_process.file_access_analyzer_process_main,
        args=('file_access_events_queue1', config_file_path, event_1)
    )

    process_2 = mp.Process(
        target=file_access_analyzer_process.file_access_analyzer_process_main,
        args=('file_access_events_queue2', config_file_path, event_2)
    )
    
    process_1.start() 
    logging.debug("Successfully started File Analysing Process 1")
    process_2.start() 
    logging.debug("Successfully started File Analysing Process 2")
    event_1.wait() 
    logging.debug("Successfully initialized the EBPF object for process 1")
    event_2.wait() 
    logging.debug("Successfully initialized the EBPF object for process 2")

    return [process_1, process_2]


def combine_file_access_results(): 
    ''' After all processes were exited correctly, tracked file access for
        subprocesses must be combined.

        Default location:  ./file_access_log_analysis/results
        Location can be changed in the configuration file. 
        This method alters the global file_access_lists.
    '''
    # TODO: Outsource to config file
    location = "./file_access_log_analysis/results"
    files = [ os.path.join(location, file) for file in os.listdir(location) if os.path.isfile(os.path.join(location, file)) ]

    logging.debug("Combining Results of file access analysis processes")

    for file in files: 
        with open(file, 'r') as json_fd: 
            data = json.load(json_fd)
            file_access_lists.file_access_read.extend(data['read'])
            file_access_lists.file_access_write.extend(data['write'])
            file_access_lists.file_access_execute.extend(data['execute'])


def init_pid_shm():
    ''' Initialization of the shared memory.

        This step has to be done centrally from the Parent PID, 
        because resource trackers get inherited by child processes. 
        The shared memory module only provides correct shm memory handling
        for this type of scenario. 
    ''' 
    global pid_shm
    
    # Initalization Shared Memory 
    array_size = np.zeros(1000, dtype=np.int64).nbytes
    pid_shm = shared_memory.SharedMemory(name="pid_shm", 
                                            create= True, size= array_size)
    
    pid_list = np.ndarray(1000, dtype=np.int64, buffer=pid_shm.buf)
    pid_list[:] = np.zeros(1000, dtype=np.int64)[:]


def init_pid_generation_process(config: configparser.ConfigParser)->mp.Process: 
    ''' Initialize PID Generation process

        This process disables SIGTERM before importing the new package for the
        child process signal handler to be initialized correctly. 
    '''
    bpf_setup_event = mp.Event() 
    old_signal = signal.signal(signal.SIGTERM, signal.SIG_IGN)
    from pid_generation_analysis import pid_generation_analysis_main

    pid_generation_process = mp.Process(
        target=pid_generation_analysis_main.pid_generation_analysis_main,
        args=(bpf_setup_event,config,))
    
    pid_generation_process.start() 
    logging.warning("Successfully started PID Generation process")
    bpf_setup_event.wait() 
    logging.warning("Successfully initialized PID Generation bpf object")

    signal.signal(signal.SIGTERM, old_signal)
    return pid_generation_process


def perform_further_argument_completion(config: configparser.ConfigParser, source_file_path): 
    logging.debug("Performing further argument completion")
    bin_name = config['PREEVALUATION']['bin_path']
    
    fuzzing_root_location = config['AFL']['rootlocation_custom_mutator']
    result_list = extract_strings.extract_params_from_bin_string_output(bin_name) 

    value_list = extract_strings.extract_string_source(source_file_path, bin_name, result_list)

    result_list = extract_strings.assign_values_to_params(value_list, result_list, fuzzing_root_location)

    arg_finder_result_file = "/synced_folder/sandboxpol/ebpf/fuzzzing_with_ebpf/arg_finder/fuzzin_string.txt"
    extract_strings.combine_different_argument_lists(result_list, arg_finder_result_file)


def dump_results(bin_basename: str): 
    global syscall_bitvector_file
    result_file_path = f"./results/{bin_basename}_policy_objects.json"    

    dictionary_results = dict() 

    dictionary_results['syscalls'] = list()

    with open(syscall_bitvector_file, 'r') as syscall_filter_bitmask_stream:
        bitmask = io.StringIO(syscall_filter_bitmask_stream.read().replace('\n'
        , ''))
        syscall_bitmask = BitVector.BitVector(fp=bitmask)

    for i in range(460): 
        try: 
            if syscall_bitmask[i]: 
                dictionary_results['syscalls'].append(seccomp.resolve_syscall(seccomp.Arch.X86_64, i).decode())
        except ValueError: 
            continue

    dictionary_results["file_access"] = dict()
    dictionary_results["file_access"]["read"] = file_access_lists.file_access_read
    dictionary_results["file_access"]["write"] = file_access_lists.file_access_write
    dictionary_results["file_access"]["execute"] = file_access_lists.file_access_execute

    dictionary_results["removed_file_access"] = dict() 
    dictionary_results["removed_file_access"]["read"] = file_access_lists.file_access_removed["read"]
    dictionary_results["removed_file_access"]["write"] = file_access_lists.file_access_removed["write"]
    dictionary_results["removed_file_access"]["execute"] = file_access_lists.file_access_removed["execute"]


    with open(result_file_path, 'w') as fd_file_path: 
        json.dump(dictionary_results, fd_file_path, indent=6)

    return dictionary_results

def main(bin_path, arg_input, fuzzing_duration, source_file_path, timeout_mode:bool =False, attach_input:str =""): 
    global b
    global syscall_bitvector_file
    global pid_shm

    config = configparser.ConfigParser() 
    config.read('./policy_generator.ini')
    adjust_config(bin_path, config)
    adjust_globals(config)

    # logging.debug(f"Starting with Symbolic Execution to gather CLI flags.")
    # execute_arg_finder(config, fuzzing_duration)

    # if source_file_path: 
    #     logging.info("Performing further argument completion steps")
    #     perform_further_argument_completion(config, source_file_path)

    # logging.debug(f"Fuzzing Main: Started with Bin_path= {bin_path}" \
    #     f" and Arg_input= {arg_input}")
    
    arg_input = "-" + arg_input
    
    adjust_fuzzing_input_file(arg_input)

    logging.debug("Going to perform normal pre-evaluation:")
    find_syscalls(config)

    initial_seccomp_tracking.initial_seccomp_tracking_main(
        syscall_bitvector_file, bin_path, arg_input, config)
    initialize_BPF(config) 
    init_pid_shm()

    logging.info("Initializing PID Generation Analysis Process")
    pid_generation_process = init_pid_generation_process(config) 
    logging.info("Initialization File Access Analysis Process")
    file_access_processes = init_file_access_processes() 
    afl_runner_thread = mp.Process(target=afl_runner_function, 
                                args=(bin_path, fuzzing_duration, timeout_mode, attach_input))

    afl_runner_thread.start() 

    b["events"].open_perf_buffer(seccomp_handler, page_cnt=256)
    while 1: 
        try:
           b.perf_buffer_poll(timeout=10*1000)
        except KeyboardInterrupt: 
           exit() 
        if not afl_runner_thread.is_alive():
            break;

    logging.info("Seccomp Analsis Process: "
        + "Finished loop for pulling events")
    afl_runner_thread.join() 
    logging.debug("Main Process: " 
        + "Sending SIGTERM to file access analyser processes")
    for process in file_access_processes: 
        process.terminate()
        process.join() 

    logging.debug("Main Process: "
        + "Sending SIGTERM to pid generation analysis process")
    pid_generation_process.terminate() 
    pid_generation_process.join()

    logging.info("Main Process: Closing and unlinking shared memory")
    pid_shm.close()
    pid_shm.unlink() 

    false_positiv_test.check_crashes_for_false_positives(config) 

    combine_file_access_results() 
    post_process_file_lists.post_process_file_access(config)

    # Generate Results in directory "./results"
    dump_results(config["KLEE"]["bin_name"])


if __name__ == "__main__": 
    logging.basicConfig(filename='fuzzer_main.log', 
        encoding='utf-8',
        level=logging.INFO,
        format='%(asctime)s %(filename)s %(funcName)s: %(message)s',
        datefmt='%H:%M:%S')
    # logging.basicConfig(level=logging.INFO,
    #                         format='%(filename)s %(funcName)s: %(message)s')
    parser = argparse.ArgumentParser(description="Main module for starting "\
        "the fuzzing process")

    parser.add_argument('-b', '--bin_path', required=True, dest="bin_path", 
        help="Path to SUT")
    parser.add_argument('-d', '--duration', default="30", dest="duration", 
        help="Duration for the fuzzing process")
    parser.add_argument('-t', '--timeout-mode', 
            action=argparse.BooleanOptionalAction,
            dest="timeout_mode",
            help="Timeout-Mode is going to terminate the SUT after 1.1 sec." +
                "This mode is especially usefull for fuzzing setup phases of " +
                "long running SUTs.")
    parser.add_argument('-i', '--input', required=True, dest="arg_input")
    parser.add_argument('-s', '--source', dest="source_file_path", 
            help="Path to the binaries source code.")
    
    parser.add_argument("-a", '--atach_input_files',
            dest='attach_input', 
            help="The input of these file is attached to the fuzzer " +
                "input corpus files")

    args = parser.parse_args() 

    main(args.bin_path, args.arg_input, args.duration,  args.source_file_path, args.timeout_mode, args.attach_input)
