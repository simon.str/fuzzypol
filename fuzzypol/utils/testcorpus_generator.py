import os 
import logging 

def generate_test_copora(cli_flag_file_name: str, 
        afl_test_case_dir: str,
        attach_files: str="")->bool:
    ''' Generate unique test case file in specified input directory for AFL.

        Parameter
        ---------
        cli_flag_file_name: str
            Filepath to the file containing the different input CLI flags.
        afl_test_case_dir: str
            Input directory of AFL, where the input corpora should be stored. 
    '''

    if (not os.path.exists(afl_test_case_dir) 
            or not os.path.exists(cli_flag_file_name)):
        logging.error("TestCorpusGenerator: CLI Flag file or "
            + "AFL test case dir is not correct")
        return False 
    
    delete_afl_test_case_dir(afl_test_case_dir)
    
    with open(cli_flag_file_name, 'r') as cli_fd: 
        flags = cli_fd.read() 

    flags = flags.split()
    counter = 0

    if not attach_files:    
        for line in flags: 
            test_corpus_file_name = os.path.join(afl_test_case_dir, 
                                                    f"corpus_{counter}.txt")
            with open(test_corpus_file_name, 'w') as test_corpus_fd: 
                test_corpus_fd.write(line)
            counter += 1 
        return True
    
    test_case_text = list() 
    
    stdin_files = [f for f in os.listdir(attach_files) if os.path.isfile(os.path.join(attach_files, f))]    
    stdin_files = [os.path.join(attach_files, file) for file in stdin_files]
    for file in stdin_files: 
        with open(file, 'rb') as fd:
            test_case_text.append(fd.read())
    
    

    for line in flags: 
            for stdin in test_case_text:
                test_corpus_file_name = os.path.join(afl_test_case_dir, 
                                                    f"corpus_{counter}.txt")
                input_tc = line.encode() + ((1000-len(line.encode()))*b' ') + stdin
                with open(test_corpus_file_name, 'wb') as test_corpus_fd: 
                    test_corpus_fd.write(input_tc)
                counter += 1 

    logging.info("Generated all test corpus files from arg file")
    return True 


def delete_afl_test_case_dir(afl_test_case_dir: str)->bool:
    ''' Function to delete all test cases in the specified afl_test_case_dir
    '''

    if not os.path.exists(afl_test_case_dir):
        logging.error("Specified AFL test case dir does not exist")
        return False 

    test_corpus_list = os.listdir(afl_test_case_dir)
    for test_corpus_file in test_corpus_list: 
        os.remove(os.path.join(afl_test_case_dir, test_corpus_file))
    

    logging.debug("Deleted all corpus files")
    return True

if __name__ == "__main__":
    generate_test_copora("/synced_folder/sandboxpol/ebpf/fuzzzing_with_ebpf/arg_finder/fuzzin_string.txt", "../new_test_candidate/in")
    
