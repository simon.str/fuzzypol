# FuzzyPol
This project provides the source code for automatically generating sandbox 
policies. Syscalls and file access of target binaries is recorded during fuzzing. 


## Licensing Terms 
The source code in this project is released under the terms of the GNU 
General Public License v2. 
