#include <uapi/linux/ptrace.h>
#include <uapi/linux/limits.h>
#include <linux/sched.h>
#include <linux/binfmts.h>

struct data_t {
    int syscall; 
    u32 pid; 
    u32 ppid; 
    u32 uid; 
    char comm[TASK_COMM_LEN]; 
    int ret_val; 
    int dirfd; 
    char pathname[NAME_MAX];
    int flags; 
}; 

//BPF_PERF_OUTPUT(file_access_events);
BPF_PERF_OUTPUT(file_access_events_queue1);
BPF_PERF_OUTPUT(file_access_events_queue2);
//PERF_EVENT_ARRAY_DECLARATION

//Not Tested
KRETFUNC_PROBE(__x64_sys_open, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 2;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);
    data.flags = PT_REGS_PARM3(regs); 

    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_stat, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 4;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_lstat, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 6;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_access, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 21;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

// Working execve tracking
KRETFUNC_PROBE(security_bprm_creds_for_exec, struct linux_binprm* bprm,  int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 
    const char *filename = bprm->filename; 
    char filepath[NAME_MAX]; 
    struct task_struct *task;
    struct data_t data = {}; 
    bpf_probe_read_kernel_str(&data.pathname, sizeof(data.pathname), (void *) filename);

    UID_FILTER
    PID_FILTER
    

    data.syscall = 59;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_truncate, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 76;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_rename, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 82;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_creat, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 85;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_link, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 86;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_unlink, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 87;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_symlink, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 88;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_readlink, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 89;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_chmod, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 90;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_chown, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 92;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_lchown, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 94;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_utime, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 132;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_mknod, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 133;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_setxattr, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 188;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_lsetxattr, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 189;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_getxattr, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 191;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_lgetxattr, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 192;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_listxattr, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 194;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_llistxattr, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 195;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_removexattr, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 197;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

//Not Tested
KRETFUNC_PROBE(__x64_sys_lremovexattr, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 198;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char *filename = (char*)PT_REGS_PARM1(regs);
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}

KRETFUNC_PROBE(__x64_sys_openat, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 257;

    data.pid = pid;     

    // task = (struct task_struct *) bpf_get_current_task(); 
    // data.ppid = task->real_parent->tgid;


    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    data.dirfd = PT_REGS_PARM1(regs);
    const char __user *filename = (char *)PT_REGS_PARM2(regs); 
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);
    data.flags = PT_REGS_PARM3(regs); 

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }

    // struct open_how __user how;
    // bpf_probe_read_user(&how, sizeof(struct open_how), (struct open_how*)PT_REGS_PARM3(regs));
    // data.mode = how.flags;
    // data.mode = PT_REGSPARM
    return 0; 
}

KRETFUNC_PROBE(__x64_sys_mknodat, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 257;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    data.dirfd = PT_REGS_PARM1(regs);
    const char __user *filename = (char *)PT_REGS_PARM2(regs); 
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);
    data.flags = PT_REGS_PARM3(regs); 

    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }

    return 0; 
}


KRETFUNC_PROBE(__x64_sys_fchownat, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 257;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    data.dirfd = PT_REGS_PARM1(regs);
    const char __user *filename = (char *)PT_REGS_PARM2(regs); 
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);
    data.flags = PT_REGS_PARM5(regs); 
    
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }

    return 0; 
}


KRETFUNC_PROBE(__x64_sys_newfstatat, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 262;

    data.pid = pid;     

    //task = (struct task_struct *) bpf_get_current_task(); 
    //data.ppid = task->real_parent->tgid;


    data.uid = uid; 

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    data.dirfd = PT_REGS_PARM1(regs);
    const char __user *filename = (char *)PT_REGS_PARM2(regs); 
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);
    data.flags = PT_REGS_PARM4(regs); 

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}


KRETFUNC_PROBE(__x64_sys_unlinkat, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 257;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    data.dirfd = PT_REGS_PARM1(regs);
    const char __user *filename = (char *)PT_REGS_PARM2(regs); 
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);
    data.flags = PT_REGS_PARM3(regs); 
    
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }

    return 0; 
}


KRETFUNC_PROBE(__x64_sys_renameat, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 257;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    data.dirfd = PT_REGS_PARM1(regs);
    const char __user *filename = (char *)PT_REGS_PARM2(regs); 
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);
    
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }

    return 0; 
}


KRETFUNC_PROBE(__x64_sys_linkat, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 257;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    data.dirfd = PT_REGS_PARM1(regs);
    const char __user *filename = (char *)PT_REGS_PARM2(regs); 
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);
    data.flags = PT_REGS_PARM5(regs); 
    
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }

    return 0; 
}


KRETFUNC_PROBE(__x64_sys_symlinkat, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 257;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    const char __user *filename = (char *)PT_REGS_PARM1(regs); 
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);
    
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }

    return 0; 
}


KRETFUNC_PROBE(__x64_sys_readlinkat, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 257;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    data.dirfd = PT_REGS_PARM1(regs);
    const char __user *filename = (char *)PT_REGS_PARM2(regs); 
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);
    
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }

    return 0; 
}


KRETFUNC_PROBE(__x64_sys_fchmodat, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 257;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    data.dirfd = PT_REGS_PARM1(regs);
    const char __user *filename = (char *)PT_REGS_PARM2(regs); 
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);
    data.flags = PT_REGS_PARM3(regs); 
    
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }

    return 0; 
}


KRETFUNC_PROBE(__x64_sys_faccessat, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 257;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    data.dirfd = PT_REGS_PARM1(regs);
    const char __user *filename = (char *)PT_REGS_PARM2(regs); 
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);
    data.flags = PT_REGS_PARM3(regs); 
    
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }

    return 0; 
}



KRETFUNC_PROBE(__x64_sys_renameat2, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 257;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    data.dirfd = PT_REGS_PARM1(regs);
    const char __user *filename = (char *)PT_REGS_PARM2(regs); 
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);
    data.flags = PT_REGS_PARM5(regs); 
    
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }

    return 0; 
}


KRETFUNC_PROBE(__x64_sys_statx, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 332;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    data.dirfd = PT_REGS_PARM1(regs);
    const char __user *filename = (char *)PT_REGS_PARM2(regs); 
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);
    data.flags = PT_REGS_PARM3(regs); 

    //bpf_trace_printk("Filename: %s      PID: %d",filename, pid); 
    //file_access_events.perf_submit(ctx, &data, sizeof(data));
    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}


KRETFUNC_PROBE(__x64_sys_chdir, struct pt_regs *regs, int ret) {
    u64 id = bpf_get_current_pid_tgid();
    u32 pid = id >> 32;
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    PID_FILTER
    struct task_struct *task;
    struct data_t data = {}; 

    data.syscall = 80;

    data.pid = pid;     

    data.uid = uid;

    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    data.ret_val = ret; 

    data.dirfd = -1;
    const char __user *filename = (char *)PT_REGS_PARM1(regs); 
    bpf_probe_read_user(&data.pathname, sizeof(data.pathname), (void *)filename);
    data.flags = 0; 

    if((data.pid%2)==0){
        file_access_events_queue1.perf_submit(ctx, &data, sizeof(data));
    } else {
        file_access_events_queue2.perf_submit(ctx, &data, sizeof(data));
    }
    return 0; 
}