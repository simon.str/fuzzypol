#include <uapi/linux/ptrace.h>
#include <linux/sched.h>
//#include <linux/fs.h>

struct val_t {
    u64 id; 
    int nr; 
};

struct data_t {
    int syscall; 
    u32 pid; 
    u32 ppid; 
    u32 uid; 
    char comm[TASK_COMM_LEN]; 
    int seccomp_action; 
}; 

BPF_PERF_OUTPUT(events);
//BPF_HASH(infotmp, u64, struct val_t);
BPF_TABLE("lru_hash", u64, struct val_t, infotmp, 1024);

int seccomp_run_filters(struct pt_regs *ctx,
    const struct seccomp_data *sd, 
    const struct seccomp_filter **match) {

    struct val_t tmp = {}; 
    
    u64 id = bpf_get_current_pid_tgid();  
    tmp.id = id; 

    tmp.nr = sd->nr;        
    
    infotmp.update(&id, &tmp); 
    //bpf_trace_printk("Return Value SECCOMP RUN_FILTER: %d\\n", tmp.nr);

    return 0; 
}

int ret_seccomp_run_filters(struct pt_regs *ctx) {
    int seccomp_action = PT_REGS_RC(ctx);
    //Remove Element from Hash Map at this point
    if(seccomp_action == SECCOMP_RET_ALLOW) {
        return 0; 
    }

    bpf_trace_printk("Return Value SECCOMP RUN_FILTER:\\n");

    struct val_t *tmp; 
    struct data_t data = {}; 
    struct task_struct *task; 

    u64 id = bpf_get_current_pid_tgid(); 

    tmp = infotmp.lookup(&id); 
    
    // Missed functions should also erase element from Hash Map
    if (tmp==0) {
        return 0;
    }
    data.pid = id >> 32; 
    data.syscall = tmp->nr; 

    task = (struct task_struct *) bpf_get_current_task(); 
    data.ppid = task->real_parent->tgid;

    data.uid = bpf_get_current_uid_gid() & 0xffffffff;
    bpf_get_current_comm(&data.comm, sizeof(data.comm));
    data.seccomp_action = seccomp_action; 

    bpf_trace_printk("Triggered function: %d\\n",data.syscall);
    

    events.perf_submit(ctx, &data, sizeof(data)); 
    return 0; 
}