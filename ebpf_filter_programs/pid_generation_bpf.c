#include <uapi/linux/ptrace.h>
#include <linux/sched.h>
#include <linux/fs.h>

#define ARGSIZE  128

struct pid_data_t {
    int syscall; 
    int pid; 
    int ppid; 
    int ret_val;
    char comm[TASK_COMM_LEN];
};

//ifndef KERNEL_RING_BUFFER
//#define KERNEL_RING_BUFFER
BPF_PERF_OUTPUT(pid_ring_buffer);
//#endif


int do_ret_sys_clone(struct pt_regs *ctx) 
{
    struct pid_data_t data = {};
    struct task_struct *task; 
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    data.syscall = 56;

    data.pid = bpf_get_current_pid_tgid() >> 32; 

    task = (struct task_struct *) bpf_get_current_task(); 
    data.ppid = task->real_parent->tgid; 

    bpf_get_current_comm(data.comm, sizeof(data.comm));
    
    data.ret_val = PT_REGS_RC(ctx);
    pid_ring_buffer.perf_submit(ctx, &data, sizeof(struct pid_data_t));
    return 0; 
}

int do_ret_sys_fork(struct pt_regs *ctx) {
    struct pid_data_t data = {}; 
    struct task_struct *task; 
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    data.syscall = 57; 
    
    data.pid = bpf_get_current_pid_tgid() >> 32; 

    task = (struct task_struct *) bpf_get_current_task(); 
    data.ppid = task->real_parent->tgid; 

    bpf_get_current_comm(data.comm, sizeof(data.comm));

    data.ret_val = PT_REGS_RC(ctx); 
    pid_ring_buffer.perf_submit(ctx, &data, sizeof(struct pid_data_t)); 
    return 0; 
}

int do_ret_sys_vfork(struct pt_regs *ctx) {
    struct pid_data_t data = {}; 
    struct task_struct *task; 
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    data.syscall = 58; 
    
    data.pid = bpf_get_current_pid_tgid() >> 32; 

    task = (struct task_struct *) bpf_get_current_task(); 
    data.ppid = task->real_parent->tgid; 

    bpf_get_current_comm(data.comm, sizeof(data.comm));

    data.ret_val = PT_REGS_RC(ctx); 
    pid_ring_buffer.perf_submit(ctx, &data, sizeof(struct pid_data_t)); 
    return 0; 
}

int do_ret_sys_clone3(struct pt_regs *ctx) {
    struct pid_data_t data = {}; 
    struct task_struct *task; 
    u32 uid = bpf_get_current_uid_gid() & 0xffffffff; 

    UID_FILTER
    data.syscall = 435; 
    
    data.pid = bpf_get_current_pid_tgid() >> 32; 

    task = (struct task_struct *) bpf_get_current_task(); 
    data.ppid = task->real_parent->tgid; 

    bpf_get_current_comm(data.comm, sizeof(data.comm));

    data.ret_val = PT_REGS_RC(ctx); 
    pid_ring_buffer.perf_submit(ctx, &data, sizeof(struct pid_data_t)); 
    return 0; 
}