#define _DEFAULT_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <seccomp.h> 
#include <sys/prctl.h>
#include <sys/types.h>


int main(int argc, char** argv) {
    scmp_filter_ctx ctx86_64 = NULL; 
    ctx86_64 = seccomp_init(SCMP_ACT_LOG); 

    prctl(PR_SET_NO_NEW_PRIVS, 1);

    if(!ctx86_64){
        perror("Eror initializing filter"); 
    }

    pid_t child_pid = fork(); 
    if(child_pid==0){
        int rc = seccomp_load(ctx86_64); 
        
        execv(argv[1], &argv[1]); 

        seccomp_release(ctx86_64); 
    }

    return 0; 
}