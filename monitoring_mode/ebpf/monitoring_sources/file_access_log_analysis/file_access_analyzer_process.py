#!/usr/bin/python3

import multiprocessing
import logging
import bcc
import os 
import json 
from multiprocessing import shared_memory
import numpy as np 
from file_access_log_analysis import file_access_lists, analyse_file_access_log

bpf_object: bcc.BPF 
perf_event_array_name: str
interrupt = True
pid_shm: shared_memory.SharedMemory
global_pid_list: np.ndarray
binary_basename: str
perf_event_array_name: str

ebpf_filter_text = ("/synced_folder/sandboxpol/ebpf/monitoring_mode/ebpf"
                        + "/ebpf_filter_programs/file_access_bpf.c")


def build_PID_filter(program_text:str) -> str:
    pid_filter_string = "if ("
    pids_noisy_programs = get_pids() 

    if not pids_noisy_programs: 
        program_text = program_text.replace("PID_FILTER", "") 
        return program_text
    else: 
        for item in pids_noisy_programs: 
            pid_filter_string += f"(pid == {item}) || "

    pid_filter_string = pid_filter_string[:-4]
    pid_filter_string += ") return 0;" 
    program_text = program_text.replace("PID_FILTER", pid_filter_string)

    return program_text

def post_process_file_access_syscall(event:bcc.table)->dict:
    result_dict = dict() 
    result_dict["syscall"] = event.syscall 
    result_dict["pid"] = event.pid 
    result_dict["ppid"] = event.ppid 
    result_dict["uid"] = event.uid 
    result_dict["comm"] = event.comm 
    result_dict["ret_val"] = event.ret_val 
    result_dict["dirfd"] = event.dirfd 
    result_dict["pathname"] = event.pathname.decode()
    result_dict["flags"] = event.flags 

    return result_dict

def get_pids()-> list:
    proc_dirs = os.listdir("/proc")
    actual_process_dirs = list() 
    for item in proc_dirs: 
        try: 
            actual_process_dirs.append(int(item))
        except ValueError:
            continue
    
    actual_process_dirs.remove(os.getpid())

    return actual_process_dirs 


def init_pid_generation_shm():
    global pid_shm
    global global_pid_list 
    
    try:    
        pid_shm = shared_memory.SharedMemory(name="pid_shm")
    except FileNotFoundError:
        logging.error("Could not initiliaze shared memory")
        exit(-1) 
    global_pid_list = np.ndarray(1000, dtype=np.int64, buffer=pid_shm.buf)
    logging.debug("File Access Analysis Process: Attached to shared memory")


def close_shared_memory(): 
    global pid_shm
    global global_pid_list

    del global_pid_list
    try: 
        pid_shm.close()
        logging.debug(f"{perf_event_array_name} " 
            + "Closing Shared Memory was successfull")
    except: 
        logging.warning(f"{perf_event_array_name} " + 
            + "Closing Shared Memory was not successful")


def init_BPF():
    global bpf_object
    
    with open(ebpf_filter_text, "r") as program_src_file: 
        program_text = program_src_file.read() 

    program_text=build_PID_filter(program_text)

    program_text = program_text.replace("UID_FILTER", "")

    cflag_list = list()
    cflag_list.append("-Wno-macro-redefined")
    logging.debug(f"{perf_event_array_name}: Disabled macro warnings")
    
    debug = 0

    bpf_object = bcc.BPF(text=program_text, cflags=cflag_list, debug=debug)
    logging.debug(f"{perf_event_array_name}:" 
        + "Successfully initialized according BPF object")


def analyze_file_access_event(cpu, data, size):
    global bpf_object 

    event = bpf_object[perf_event_array_name].event(data) 

    perf_event = post_process_file_access_syscall(event)

    if perf_event["syscall"] == 81: 
        logging.error(f"Found intersting case:\n{perf_event}")
        return


    # Syscall 80 (CHDIR) to consider AT_FDCWD
    if perf_event["syscall"] == 80:
        logging.debug("Syscall Analyzer: Found chdir command -> Changing dir")
        if  not perf_event["ret_val"] == 0 : 
            return 
        if  perf_event['pid'] not in file_access_lists.pid_list: 
            file_access_lists.pid_list[perf_event['pid']] = dict()
        file_access_lists.pid_list[perf_event['pid']]['AT_FDCWD'] = perf_event["pathname"] 
        return 
            

    # Case 1: PID alread tracked in local PID list -> Proceed with analysing
    if perf_event['pid'] in file_access_lists.pid_list:
        pass
    # Case 2: PID not tracked but comm matches expectations 
    # -> Add PID and continue analysing
    elif ((perf_event['pid'] not in file_access_lists.pid_list)
            and (perf_event['comm'].decode("utf-8")) == binary_basename):
        file_access_lists.pid_list[perf_event['pid']] = dict()
    # Case 3: PID not locally tracked but in global PID storage:
    # -> Add PID to local storage and continue computing
    elif (perf_event['pid'] in global_pid_list): 
        file_access_lists.pid_list[perf_event['pid']] = dict() 
    else: 
        return

    logging.debug(f"File Access: {perf_event}")
    
    analyse_file_access_log.analyse_file_access_syscall(perf_event)


def dump_results(): 
    logging.debug("Finished execution of process with perf event array "
            + f"{perf_event_array_name} with the following results:"
            + f"\n\tRead= {file_access_lists.file_access_read}"
            + f"\n\tWrite= {file_access_lists.file_access_write}"
            + f"\n\tExecute= {file_access_lists.file_access_execute}"
            + f"\n\tPID List= {file_access_lists.pid_list}")

    file_path = os.path.join("./file_access_log_analysis/results", 
                    (perf_event_array_name + ".json"))
    concattenated_results = dict() 
    concattenated_results['read'] = file_access_lists.file_access_read
    concattenated_results['write'] = file_access_lists.file_access_write
    concattenated_results['execute'] = file_access_lists.file_access_execute

    with open(file_path, 'w') as fd_file_path: 
        json.dump(concattenated_results, fd_file_path, indent=6)


def file_access_analyzer_process_main(ringbuffer_name: str, 
        bin_basename: str,
        event: multiprocessing.Event):
    global bpf_object
    global perf_event_array_name
    global binary_basename

    global perf_event_array_name

    init_pid_generation_shm()

    perf_event_array_name = ringbuffer_name

    binary_basename = bin_basename

    init_BPF()

    event.set() 
    bpf_object[ringbuffer_name].open_perf_buffer(analyze_file_access_event, page_cnt=256)

    # Proceed to pull data from the perf ring buffer until SIGTERM or SIGINT
    # are received
    while interrupt:
        try:
            bpf_object.perf_buffer_poll(timeout=1000)
        except KeyboardInterrupt:
            break;
        
            
    
    logging.debug(f"{perf_event_array_name}: Finished Main Loop")
    dump_results()
    close_shared_memory()
    
    logging.debug(f"{perf_event_array_name}: "
        + "Successfully ended execution and done cleanup.")
