import os
import logging 
from file_access_log_analysis import  file_access_constants, file_access_lists

class Syscall: 
    __syscall_table = {
        2: 4,
        4: 1,
        6: 1,
        21: 1,
        59: 3,
        76: 2,
        82: 2,
        85: 2 ,
        86: 2,
        87: 2,
        88: 2,
        89: 1,
        90: 2,
        92: 2,
        94: 2,
        132: 2,
        133: 2,
        188: 2,
        189: 2,
        191: 1,
        192: 1,
        194: 1,
        195: 1,
        197: 2,
        198: 2,
        235: 2,
        257: 4,
        259: 2,
        260: 2,
        262: 1,
        263: 2,
        264: 2,
        265: 2,
        266: 2,
        267: 1,
        268: 2,
        269: 1,
        316: 2,
        322: 3,
        332: 1
    }

    __fd_returning_syscalls = [2, 85, 257, 437]

    def __init__(self, result_dict:dict) -> None:
        self.syscall_nr = result_dict['syscall']
        self.pid = result_dict['pid']
        self.ret_val = result_dict['ret_val']
        self.dirfd = result_dict['dirfd']
        self.pathname = result_dict['pathname']
        self.flags = result_dict['flags']
        self.accmode = ""
        #if self.syscall_nr in self.__fd_returning_syscalls: 
            #logging.error(f"Complete Dictionary:\n\t{result_dict}")

        self.__evaluate_access_mode() 

    def __evaluate_access_mode(self): 
        # If the accmode is already determined by the syscall itself
        # only the __syscall table lookup has to be performed 
        if Syscall.__syscall_table[self.syscall_nr] != 4: 
            self.accmode = Syscall.__syscall_table[self.syscall_nr]
        elif (self.flags & os.O_ACCMODE) == os.O_RDONLY: 
            self.accmode = 1
        elif (((self.flags & os.O_ACCMODE) == os.O_WRONLY)
            or ((self.flags & os.O_ACCMODE) == os.O_RDWR)): 
            self.accmode = 2
        else: 
            self.accmode = 3

    def _fd_ret_value(self):
        if self.syscall_nr in Syscall.__fd_returning_syscalls: 
            return True 
        else: 
            return False 

    def _add_to_tracked_files(self, pathname): 
        ''' Add pathname to according tracked file list 
            Evaluate before if file was already tracked 
        '''
        if self.accmode == 1: 
            if pathname not in  file_access_lists.file_access_read: 
                #logging.error(f"{self.syscall_nr} {self.dirfd} {self.pathname} {self.flags} {self.ret_val}")
                file_access_lists.file_access_read.append(pathname)
        elif self.accmode == 2: 
            if pathname not in  file_access_lists.file_access_write: 
                file_access_lists.file_access_write.append(pathname)
        else: 
            if pathname not in  file_access_lists.file_access_execute: 
                file_access_lists.file_access_execute.append(pathname)


class NonFDSyscall(Syscall): 
    ''' Class to process all NonFD related file access syscalls 
    '''
    def process_file_access(self):
        if self.ret_val < 0:
            return

        self._add_to_tracked_files(self.pathname) 

        # This case only fits open, which is not able to work based on FDs but
        # returns an FD
        if self._fd_ret_value(): 
            file_access_lists.pid_list[self.pid][self.ret_val] = self.pathname
        

class FDSyscall(Syscall): 
    ''' Class that is able to process syscalls which can have file descriptors
        as arguments. However, these syscalls do not necessarily return a 
        file descriptor back 
    '''

    def process_file_access(self): 
        if self.ret_val < 0: 
            return 

        if (not self.pathname): 
            # For no given Pathname high possibility for AT_EMPTY_PATH
            if ((self.flags & file_access_constants.AT_EMPTY_PATH) 
                    == file_access_constants.AT_EMPTY_PATH):
                return 
        elif self.pathname[0] == "/": 
            # Trivial Case: Absolute Path given 
            self._add_to_tracked_files(self.pathname) 
            if super()._fd_ret_value(): 
                file_access_lists.pid_list[self.pid][self.ret_val] = self.pathname
        elif self.dirfd == file_access_constants.AT_FDCWD:
            # Pathname -100 evaluates to current working directory 
            if "AT_FDCWD" in file_access_lists.pid_list[self.pid]:
                pathname = os.path.join(
                            file_access_lists.pid_list[self.pid]["AT_FDCWD"], 
                            self.pathname)
                #logging.error(f"Now entering the new case\t{pathname}")
            else: 
                pathname = os.path.join(os.getcwd(), self.pathname)
            
            self._add_to_tracked_files(pathname)
            if self._fd_ret_value(): 
                file_access_lists.pid_list[self.pid][self.ret_val] = pathname 
        else: 
            # Complex case: Path is joined from FD and relative Filename to it 
            pathname = os.path.join(
                file_access_lists.pid_list[self.pid][self.dirfd], 
                self.pathname)
            self._add_to_tracked_files(pathname)
            if self._fd_ret_value(): 
                file_access_lists.pid_list[self.pid][self.ret_val] = pathname 
