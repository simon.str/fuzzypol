#!/usr/bin/env python3 
import bcc 
import numpy as np 
import multiprocessing as mp
from multiprocessing import shared_memory
import signal
from file_access_log_analysis import file_access_analyzer_process, post_process_file_lists
import file_access_lists
import json 
import os 
import sys
import argparse
import logging


try: 
    import seccomp 
except: 
    import pyseccomp as seccomp


seccomp_action = {0x80000000:"SECCOMP_RET_KILL_PROCESS", 
                   0x00000000:"SECCOMP_RET_KILL_THREAD",
                   0x00030000:"SECCOMP_RET_TRAP",
                   0x00050000:"SECCOMP_RET_ERRNO", 
                   0x7fc00000:"SECCOMP_RET_USER_NOTIF",
                   0x7ff00000:"SECCOMP_RET_TRACE",
                   0x7ffc0000:"SECCOMP_RET_LOG",
                   0x7fff0000:"SECCOMP_RET_ALLOW"}

b: bcc.BPF
syscall_bitvector = np.zeros(480, dtype=np.int0)
syscall_name_list = list()
pid_shm: shared_memory.SharedMemory


def establish_bpf_filters(): 
    global b 

    seccomp_ebpf = "/synced_folder/sandboxpol/ebpf/monitoring_mode/ebpf/ebpf_filter_programs/seccomp_bpf.c"
    with open(seccomp_ebpf, 'r') as seccomp_filter_file: 
        seccomp_filter_text = seccomp_filter_file.read()

    cflag_list = list()
    cflag_list.append("-Wno-macro-redefined")
    logging.debug(f"Seccomp: Disabled macro warnings")
    
    debug = 0

    b = bcc.BPF(text=seccomp_filter_text, cflags=cflag_list, debug=debug)
    b.attach_kprobe(event="seccomp_run_filters", 
                        fn_name="seccomp_run_filters")
    b.attach_kretprobe(event="seccomp_run_filters",
                        fn_name="ret_seccomp_run_filters")
    
    logging.debug("Established SECCOMP EBPF filters.")


def post_process_seccomp_events(event)-> dict:
    ''' Extract the event data of seccomp events and return an according 
        dictionary. 

        Available entries:
            result_dict["syscall"]\n
            result_dict["pid"]\n
            result_dict["ppid"]\n
            result_dict["uid"]\n
            result_dict["seccomp_action"]\n
            result_dict["comm"]\n
    '''
    global seccomp_action
    result_dict = dict()

    result_dict["syscall"] = event.syscall
    result_dict["pid"] = event.pid
    result_dict["ppid"] = event.ppid
    result_dict["uid"] = event.uid
    result_dict["seccomp_action"] = seccomp_action[abs(event.seccomp_action)]
    result_dict["comm"] = event.comm.decode("utf-8")

    return result_dict

def seccomp_handler(cpu, data, size):
    ''' Callback function for pulling events through the ring buffer. 
    '''
    global b 
    global syscall_bitvector
    event = b["events"].event(data)
    result_dict = post_process_seccomp_events(event)

    if (result_dict['seccomp_action'] == "SECCOMP_RET_LOG" 
            and syscall_bitvector[result_dict['syscall']] == 0):

        syscall_bitvector[result_dict['syscall']] = 1
        logging.debug(f"SECCOMP: {result_dict}")    


def print_results(result_dict:dict): 
    modes = ["read", "write", "execute"]

    print("\n\nResults of Policy Analysis:")

    print("\nOverview File Access:")

    for mode in modes: 
        print(f"\tAccess Mode= {mode}")
        for file in result_dict["file_access"][mode]:
            print(f"\t\t{file}")

    
    print("\nOverview found Syscalls:")
    for syscall in result_dict["syscalls"]: 
        print(f"\t{syscall}")


def init_pid_shm():
    global pid_shm
    
    array_size = np.zeros(1000, dtype=np.int64).nbytes
    pid_shm = shared_memory.SharedMemory(name="pid_shm", 
                                            create= True, size= array_size)
    
    pid_list = np.ndarray(1000, dtype=np.int64, buffer=pid_shm.buf)
    pid_list[:] = np.zeros(1000, dtype=np.int64)[:]
    logging.debug("Initalized Shared Memory for PID Map.")


def init_pid_generation_process(bin_basename)->mp.Process: 
    bpf_setup_event = mp.Event() 
    old_signal = signal.signal(signal.SIGTERM, signal.SIG_IGN)
    from pid_generation_analysis import pid_generation_analysis_main

    pid_generation_process = mp.Process(
        target=pid_generation_analysis_main.pid_generation_analysis_main,
        args=(bpf_setup_event,bin_basename,))
    
    pid_generation_process.start() 
    
    bpf_setup_event.wait() 

    signal.signal(signal.SIGTERM, old_signal)
    return pid_generation_process



def init_file_access_processes(bin_basename)-> list: 
    event_1 = mp.Event() 
    event_2 = mp.Event() 

    process_1 = mp.Process(
        target=file_access_analyzer_process.file_access_analyzer_process_main,
        args=('file_access_events_queue1', bin_basename, event_1)
    )

    process_2 = mp.Process(
        target=file_access_analyzer_process.file_access_analyzer_process_main,
        args=('file_access_events_queue2', bin_basename, event_2)
    )
    
    process_1.start() 
    logging.info("Successfully started File Analysing Process 1")
    process_2.start() 
    logging.info("Successfully started File Analysing Process 2")
    event_1.wait() 
    logging.info("Successfully initialized the EBPF object for process 1")
    event_2.wait() 
    logging.info("Successfully initialized the EBPF object for process 2")

    return [process_1, process_2]


def dump_results(bin_basename: str): 
    result_file_path = f"./results/{bin_basename}_policy_objects.json"    

    dictionary_results = dict() 

    dictionary_results['syscalls'] = syscall_name_list

    for syscall_index in range(480):
        if syscall_bitvector[syscall_index]: 
            syscall_name = seccomp.resolve_syscall(seccomp.Arch.X86_64, 
                                syscall_index).decode()
            if syscall_name not in dictionary_results['syscalls']: 
                dictionary_results['syscalls'].append(syscall_name)

    dictionary_results["file_access"] = dict()
    dictionary_results["file_access"]["read"] = file_access_lists.file_access_read
    dictionary_results["file_access"]["write"] = file_access_lists.file_access_write
    dictionary_results["file_access"]["execute"] = file_access_lists.file_access_execute

    with open(result_file_path, 'w') as fd_file_path: 
        json.dump(dictionary_results, fd_file_path, indent=6)
    

    return dictionary_results

def combine_file_access_results(): 
    location = "./file_access_log_analysis/results"
    files = [ os.path.join(location, file) for file in os.listdir(location) 
                if os.path.isfile(os.path.join(location, file)) ]

    logging.info("Combining Results of file access analysis processes")

    for file in files: 
        with open(file, 'r') as json_fd: 
            data = json.load(json_fd)
            file_access_lists.file_access_read.extend(data['read'])
            file_access_lists.file_access_write.extend(data['write'])
            file_access_lists.file_access_execute.extend(data['execute'])

def monitoring_main(bin_basename: str, result_file_path: str=""): 
    global pid_shm

    if result_file_path: 
        if not os.path.exists(result_file_path):
            logging.error("JSON File path does not exist!")
            sys.exit(-1) 
        read_current_state(result_file_path)

    establish_bpf_filters() 

    init_pid_shm() 
    pid_generation_process = init_pid_generation_process(bin_basename) 
    file_access_processes = init_file_access_processes(bin_basename) 

    b["events"].open_perf_buffer(seccomp_handler, page_cnt=256)
    while 1: 
        try:
           b.perf_buffer_poll(timeout=10*1000)
        except KeyboardInterrupt: 
           break; 

    for process in file_access_processes:
        process.join() 

    pid_generation_process.terminate() 
    pid_generation_process.join() 

    
    pid_shm.close()
    pid_shm.unlink() 

    combine_file_access_results()
    post_process_file_lists.post_process_file_access()
    result_dict = dump_results(bin_basename)
    print_results(result_dict)


def read_current_state(result_file_path: str): 
    global syscall_bitvector
    global syscall_name_list

    logging.info("Reading Previous State from JSON file")

    with open(result_file_path, 'r') as result_file_stream: 
        current_result_state = json.load(result_file_stream)

    expected_keys = ['syscalls', 'file_access', 'removed_file_access']

    if expected_keys != list(current_result_state):
        logging.error("Given result JSON is malformed!")
        sys.exit(-1) 

    file_access_lists.file_access_read = current_result_state['file_access']['read']
    file_access_lists.file_access_write = current_result_state['file_access']['write']
    file_access_lists.file_access_execute = current_result_state['file_access']['execute']

    for syscall in current_result_state['syscalls']: 
        syscall_name_list.append(syscall)


if __name__ == "__main__": 
    parser = argparse.ArgumentParser(description="Policy Monitoring Tool: "
        + "This tool monitors syscall and file access usage of any binary. " 
        + "The binaries can also be long running userspace applications with "
        + "GUI interfaces. All results will be published as JSON files"
        + " in './results'")    

    parser.add_argument('-b', '--basename', dest="bin_basename", required=True,
        type=str, help='Basename of the binary for Comm Name matching.')
    parser.add_argument('-d', '--debug', action=argparse.BooleanOptionalAction,
        help='Show all received messages from all kernel ring buffers')
    parser.add_argument('-u', '--update', dest="result_file_path", 
        help='Update results from previous fuzzing.' 
            + ' An file path to the result JSON is expected')
    args = parser.parse_args() 

    if args.debug: 
        logging_level = logging.DEBUG
    else: 
        logging_level = logging.INFO

    logging.basicConfig(level=logging_level,
                            format='%(filename)s %(funcName)s: %(message)s')
    logging.info(f"Started the Monitoring Tool for Policy Generation with"
        + f" COMM name {args.bin_basename}")
    
    monitoring_main(args.bin_basename, args.result_file_path)