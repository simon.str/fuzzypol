#!/usr/bin/env python3
# encoding: utf-8

import random
import select
import string
import os 
import configparser
import subprocess
import config_class
import time 
import logging 

post_process_flag = False 

mutator_config: config_class.WeightedInputMutatorConfig


def init(seed):
    global mutator_config
    random.seed(seed)

    config = configparser.ConfigParser()
    file_path = os.path.abspath(__file__) 
    file_path = os.path.dirname(file_path)
    config_file = os.path.join(file_path, "custom_mutator.ini")
    config.read(config_file)

    mutator_config = config_class.WeightedInputMutatorConfig(config)

    print(f"Add_operation= {mutator_config.add_operation}\n"
                    + f"Delete Operation= {mutator_config.delete_operation}\n"
                    + f"Argument Flag Value= {mutator_config.argument_flag_value}\n"
                    + f"Random Arg Vector= {mutator_config.random_arg_vector}")


def deinit():
    pass


def fuzz(buf, add_buf, max_size)-> bytearray:
    global post_process_flag
    post_process_flag = False

    atomic_operation_pick = random.randint(1,100)

    match atomic_operation_pick: 
        case atomic_operation_pick if 0 <= atomic_operation_pick < mutator_config.add_operation:
            ret_value = combined_add(buf)
        case atomic_operation_pick if mutator_config.add_operation <= atomic_operation_pick < mutator_config.delete_operation: 
            ret_value = delete_corpus_input_component(buf)
        case atomic_operation_pick if mutator_config.delete_operation <= atomic_operation_pick <= mutator_config.argument_flag_value:
            index_value = has_flag(buf)
            if random.randint(0,1) == 0:
                if index_value != -1:
                    ret_value = replace_existing_flag_arg(buf, index_value)
                else:
                    ret_value = add_rand_arg_flag(buf)
            else: 
                ret_value = add_non_existing_rand_arg_flag(buf) 
        case _: 
            ret_value = random_argument_vector()

    ret_value += bytearray(((1000 - len(ret_value) - 2) * ' ').encode())
    # if  mutator_config.consider_stdin: 
    #     number_of_letters = random.randint(1, 10) 
    #     rand_input_string = ""
    #     for i in range(number_of_letters): 
    #             rand_input_string += random.choice(mutator_config.set_of_valid_characters) 

    #     ret_value += bytearray("\0\0".encode())
    #     rand_input_string = bytearray(rand_input_string.encode())
    #     ret_value += rand_input_string

    ret_value = ret_value + buf[1000:] 

    return ret_value


# def fuzz_count(buf):

#     return 200


def random_argument_vector()-> bytearray:
    choice = random.randint(0,8)

    match choice: 
        case 0: 
            ret_value = rand_input_with_file()
        case choice if 1 <= choice < 5: 
            ret_value = existing_flag()
        case choice if 5 <= choice < 7: 
            ret_value = existing_flag_with_file()
        case _: 
            ret_value = existing_flag_with_multiple_files()

    return ret_value


def rand_input_with_file()-> bytearray: 
    ''' This methods provides inputs in the form of 1-3 invalid input flags and a file path. 
    ''' 
    result_string = "" 
    number_of_flags = random.randint(1, 3) 

    for i in range(number_of_flags): 
        number_of_chars= random.randint(1,4) 
        result_flag = "-"
        for i in range(number_of_chars): 
            result_flag += random.choice(string.ascii_letters) 
        result_flag += " " 
        result_string += result_flag

    result_string +=  random.choice(mutator_config.include_files).strip()    
 
    return bytearray(result_string.encode())


def existing_flag()-> bytearray: 
    ''' This methods provides inputs in the form of 1-3 valid input flags but not an existing file. 
    ''' 
    result_string = "" 
    number_of_flags = random.randint(3, 4)  

    for i in range(number_of_flags): 
        result_string = random.choice(mutator_config.valid_flags)
        result_string += " "
    
    return bytearray(result_string.encode())


def existing_flag_with_file()-> bytearray: 
    ''' This methods provides inputs in the form of 1-3 valid input flags and a file path. 
    '''
    result_string = "" 
    number_of_flags = random.randint(3, 4) 

    for i in range(number_of_flags): 
        result_string += random.choice(mutator_config.valid_flags).strip()
        result_string += " "
    
    
    result_string +=  random.choice(mutator_config.include_files)
    return bytearray(result_string.encode()) 


def existing_flag_with_multiple_files()-> bytearray: 
    ''' This methods provides inputs in the form of 1-3 valid input flags and a file path. 
    '''
    result_string = "" 
    number_of_flags = random.randint(3, 4) 

    for i in range(number_of_flags): 
        result_string += random.choice(mutator_config.valid_flags).strip()
        result_string += " "
        for x in range(2): 
            if random.randint(0,1) == 1: 
                result_string +=  random.choice(mutator_config.include_files).strip()
                result_string += " "
    
    return bytearray(result_string.encode()) 


def combined_add(current_buf: bytearray):
    number_operations = random.randint(1,5)

    ret_value = current_buf
    for i in range(number_operations):
        operation_decision = random.randint(0,2)
        
        match operation_decision: 
            case 0 | 1: 
                ret_value = add_flag(ret_value)
            case _: 
                ret_value = add_file_path(ret_value) 

    return ret_value


def add_flag(current_buf: bytearray)-> bytearray:
    new_flag = bytearray(random.choice(mutator_config.valid_flags).encode())
    # In case the generated Input would be to large to fit the requirements
    if (len(new_flag) + len(current_buf)) > 1000: 
        return delete_corpus_input_component(current_buf)
    
    ret_value = current_buf + bytearray(" ".encode()) + new_flag

    return ret_value
    

def add_file_path(current_buf:bytearray)-> bytearray:
    new_file_path = bytearray(random.choice(mutator_config.include_files).encode())
    # In case the generated Input would be to large to fit the requirements
    if (len(new_file_path) + len(current_buf)) > 1000: 
        return delete_corpus_input_component(current_buf)
    
    ret_value = current_buf + bytearray(" ".encode()) + new_file_path

    return ret_value


def delete_corpus_input_component(current_buf: bytearray)->bytearray: 
    # In case splicing messes up format and returns undecodable bytearays
    # return the input buffer
    try: 
        string_repr = current_buf.decode()
    except UnicodeDecodeError: 
        return current_buf
    # Consider possibility that too many flags/file path should be removed
    if len(string_repr.split()) <= 1:
        if len(string_repr) >= 900: 
            return current_buf
        if random.randint(0,1):
            return add_flag(current_buf)
        else: 
            return add_file_path(current_buf)

    ret_value = bytearray((' '.join(string_repr.split()[:-1])).encode())

    return ret_value


def post_process(buf):
    global post_process_flag

    if post_process_flag:
        flag = random.choice(mutator_config.valid_flags).strip() 
        bytearray_flag = bytearray(flag.encode())
        flag = random.choice(mutator_config.valid_flags).strip() 
        bytearray_flag += bytearray(flag.encode())
        
        bytearray_flag += bytearray(((1000 - len(bytearray_flag) ) * ' ').encode())
    
        if len(buf) > 1000: 
            buf = bytearray_flag + buf[1000:]
        else: 
            buf = bytearray_flag + buf

        return buf
    else:
        post_process_flag = True 
        return buf
    

# def havoc_mutation(buf, max_size):
#     global post_process_flag

#     post_process_flag = True 
#     return buf 


def has_flag(current_buf)-> int: 
    ''' Function to determine the index of the first flag specific argument. 

        In case of no appearance of flag specific arguments -1 will be returned. 
    '''
    # try: 
    #     string_repr = current_buf.decode()
    # except UnicodeDecodeError: 
    #     return current_buf

    # splitted_list = string_repr.split() 
    # number_of_items = len(splitted_list)-1

    # for item in splitted_list:
    #     if (splitted_list.index(item) != number_of_items) and (item[0] != '-'):
    #         return splitted_list.index(item)
    
    # return -1
    try: 
        string_repr = current_buf.decode()
    except UnicodeDecodeError: 
        return current_buf

    splitted_list = string_repr.split() 
    number_of_items = len(splitted_list)

    index_list = list()
    for item in splitted_list:
        if (splitted_list.index(item) != number_of_items) and ((item[0] != '-') or ("=" in item)):
            index_list.append(splitted_list.index(item))
            #print(index_list)

    #print(f"has_flag: \n{splitted_list}\n{index_list}") 
    if index_list:            
        return random.choice(index_list)
    else: 
        return -1


def replace_existing_flag_arg(current_buf: bytearray, index: int) -> bytearray: 
    try: 
        string_repr = current_buf.decode()
    except UnicodeDecodeError: 
        return current_buf

    splitted_current_buf = string_repr.split()


    if "=" in splitted_current_buf[index]:
        flag = splitted_current_buf[index].split("=")[0] + "="
        rand_flag_value =  select_argument_type() 
        flag = flag.encode() + rand_flag_value

        del splitted_current_buf[index]
        splitted_current_buf = [flag.encode() for flag in splitted_current_buf]
        splitted_current_buf.insert(index, flag)

        result = b''

        for flag in splitted_current_buf: 
            result += flag + b' '

        return bytearray(result)

    del splitted_current_buf[index]

    rand_flag_value = select_argument_type() 

    result = b''

    for i in range(len(splitted_current_buf)): 
        if i == index:
            result += rand_flag_value + b' '
        result += splitted_current_buf[i].encode() +  b' '
    
    return bytearray(result)


def add_rand_arg_flag(current_buf):
    try: 
        string_repr = current_buf.decode()
    except UnicodeDecodeError: 
        return current_buf

    splitted_current_buf = string_repr.split() 

    rand_flag_value = select_argument_type()

    try: 
        index_to_replace = random.randint(1, len(splitted_current_buf)-1)
    # Error Case: Only 1 argument given
    except ValueError:
        if len(splitted_current_buf) >= 1: 
            error_result = splitted_current_buf[0].encode() + b' ' + rand_flag_value
        else:
            return random_argument_vector()
        return bytearray(error_result)
    
    result = b''

    for i in range(len(splitted_current_buf)):
        if i == index_to_replace: 
            result += rand_flag_value + b' '

        result += splitted_current_buf[i].encode() + b' '

    return bytearray(result)


def add_non_existing_rand_arg_flag(current_buf)-> bytearray: 
    try: 
        string_repr = current_buf.decode()
    except UnicodeDecodeError: 
        return current_buf

    string_repr = string_repr.split() 
    number_flags = len(string_repr)
    try:
        index = random.randint(1, number_flags-1)
    except ValueError: 
        current_buf = current_buf + b' ' + select_argument_type()
        return current_buf
    result_string = ""

    for flag in string_repr: 
        if string_repr.index(flag) == index: 
            result_string += select_argument_type().decode()+" "
        
        result_string += flag + " "
    
    result_string = result_string.encode()
    return bytearray(result_string)


def select_argument_type()-> bytearray:
    argument_type = random.randint(0,3)

    match argument_type:
        case 0 | 1: 
            # existing str 
            rand_flag_value = random.choice(mutator_config.binary_strings)
        case 2:
            # int
            rand_flag_value = str(random.randint(0, 3000)).encode() 
        case 3: 
            # path
            rand_flag_value = random.choice(mutator_config.include_files).encode()

    return rand_flag_value    


if __name__== "__main__": 
    current_buf = b"-f /home/vagrant -g /lastfile"
    current_buf_2 = b"-f -g -d -e -a"
    init(random.randbytes(16))

    add_non_existing_rand_arg_flag(current_buf_2)
    #replace_existing_flag_arg(current_buf, 1)
    #add_rand_arg_flag(current_buf)
