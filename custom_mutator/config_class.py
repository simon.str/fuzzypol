import configparser
import os 
import string 
import subprocess

class WeightedInputMutatorConfig: 
    add_operation: int
    delete_operation:  int 
    argument_flag_value: int 
    random_arg_vector: int 
    existing_flags_path: str 
    locations_path: str
    set_of_valid_characters: str 

    include_files= list() 
    valid_flags = list() 
    binary_strings = list() 

    consider_stdin = False 

    def __init__(self, config: configparser.ConfigParser): 

        weight_counter = 100 
        WeightedInputMutatorConfig.random_arg_vector = weight_counter
        weight_counter = weight_counter - int(config["thresholds"]["random_arg_vector"])

        WeightedInputMutatorConfig.argument_flag_value = weight_counter
        weight_counter = weight_counter - int(config["thresholds"]["argument_flag_value"])

        WeightedInputMutatorConfig.delete_operation =  weight_counter
        weight_counter = weight_counter - int(config["thresholds"]["delete_operation"])
        
        WeightedInputMutatorConfig.add_operation = int(config["thresholds"]["add_operation"])
                
        
        WeightedInputMutatorConfig.existing_flags_path = config['weighted_input_mutator']['existing_flags_path']
        WeightedInputMutatorConfig.locations_path = config['weighted_input_mutator']['locations_path']


        WeightedInputMutatorConfig.consider_stdin = config.getboolean('weighted_input_mutator', 'consider_stdin') 

        with open(WeightedInputMutatorConfig.locations_path, "r") as location_file: 
            root_location = location_file.read().strip()

        for (dirpath, dirnames, filenames) in os.walk(root_location): 
            for file in filenames: 
                WeightedInputMutatorConfig.include_files.append(os.path.join(dirpath, file))

        with open(WeightedInputMutatorConfig.existing_flags_path, "r") as existing_flags_file: 
            WeightedInputMutatorConfig.valid_flags = existing_flags_file.readlines()
            WeightedInputMutatorConfig.valid_flags = [flag.strip() for flag in WeightedInputMutatorConfig.valid_flags] 


        valid_character_flag = config['weighted_input_mutator']['valid_characters']

        WeightedInputMutatorConfig.set_of_valid_characters = ""
        if valid_character_flag == 'a': 
            WeightedInputMutatorConfig.set_of_valid_characters = string.ascii_letters + string.digits
        elif valid_character_flag == 'l': 
            WeightedInputMutatorConfig.set_of_valid_characters = string.ascii_letters
        elif valid_character_flag == 'd':
            WeightedInputMutatorConfig.set_of_valid_characters = string.digits

        bin_path = config['binary_information']['bin_path']
        strings_bin_path = subprocess.check_output(["strings", "-d", bin_path])

        for item in strings_bin_path.split():
            if item.isalpha() or item.isdigit():
                WeightedInputMutatorConfig.binary_strings.append(item)
