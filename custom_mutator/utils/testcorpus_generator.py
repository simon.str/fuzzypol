import os 

def generate_test_copora(cli_flag_file_name: str, afl_test_case_dir: str)->bool:
    ''' Generate unique test case file in specified input directory for AFL.

        Parameter
        ---------
        cli_flag_file_name: str
            Filepath to the file containing the different input CLI flags.
        afl_test_case_dir: str
            Input directory of AFL, where the input corpora should be stored. 
    '''

    if (not os.path.exists(afl_test_case_dir) 
            or not os.path.exists(cli_flag_file_name)):
        print("TestCorpusGenerator: CLI Flag file or "
            + "AFL test case dir is not correct")
        return False 
    
    delete_afl_test_case_dir(afl_test_case_dir)
    
    with open(cli_flag_file_name, 'r') as cli_fd: 
        flags = cli_fd.read() 

    flags = flags.split()
    counter = 0

    for line in flags: 
        test_corpus_file_name = os.path.join(afl_test_case_dir, 
                                                f"corpus_{counter}.txt")
        with open(test_corpus_file_name, 'w') as test_corpus_fd: 
            test_corpus_fd.write(line)
        counter += 1 

    return True 


def delete_afl_test_case_dir(afl_test_case_dir: str)->bool:
    ''' Function to delete all test cases in the specified afl_test_case_dir
    '''

    if not os.path.exists(afl_test_case_dir):
        print("Specified AFL test case dir does not exist")
        return False 

    test_corpus_list = os.listdir(afl_test_case_dir)
    for test_corpus_file in test_corpus_list: 
        os.remove(os.path.join(afl_test_case_dir, test_corpus_file))
    
    return True

if __name__ == "__main__":
    generate_test_copora("/synced_folder/sandboxpol/ebpf/fuzzzing_with_ebpf/arg_finder/fuzzin_string.txt", "../new_test_candidate/in")
    
